# Learning

Coursework and learning projects that don't merit their own repositories. Organized by topic.

Projects that once existed as standalone repositories may have their `.git/` directory renamed to `_git/` or similar, to simplify storage in this parent repository without fully losing the git history.
