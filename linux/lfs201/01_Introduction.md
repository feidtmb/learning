# Course Introduction

- [Prerequisites](#prerequisites)
- [Resources](#resources)
- [Linux Distributions](#linux-distributions)

## Prerequisites

- Text editors (vi, emacs, nano, gedit, ...)
- Finding Linux documentation (getting help)
- Manipulating text (sed, grep, awk, cut, paste, tail, head, ...)
- File utilities (find, locate, file, ...)
- Printing (configuring printers, managing print jobs)
- Graphical interfaces and their administration
- bash shell scripting
- System installation

## Resources

https://training.linuxfoundation.org/cm/LFS201/

- User: `LFtraining`
- Pass: `Penguin2014`

## Linux Distributions

This course focuses on current popular Enterprise Linux distributions:

- Red Hat Enterprise Linux (**RHEL**)
  - Derived distributions include **CentOS** and **Oracle Linux**.
  - **Fedora** may be seen as the upstream for RHEL, but is cutting edge and therefore often not used in Enterprise deployments.
- **SUSE**
  - Closely related to **openSUSE** in the same way that Fedora is for RHEL.
- **Debian**
  - Derived distributions include **Ubuntu** and **Linux Mint**.

Differences between the above are most commonly related to how software is packaged.