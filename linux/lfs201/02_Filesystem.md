# Filesystem Tree Layout

- [One Big Filesystem](#one-big-filesystem)
- [Data Distinctions](#data-distinctions)
- [File Hierarchy Standard (FHS)](#file-hierarchy-standard-fhs)
- [Demo: Main Consumers Of Storage](#demo-main-consumers-of-storage)
- [Exercise: Tour `/proc`](#exercise-tour-proc)

## One Big Filesystem

All UNIX-based operating systems including Linux consist of one logical filesystem tree rooted at `/`. Within this tree there may be any number of distinct filesystems mounted at various points.

## Data Distinctions

Shareable vs. non-shareable:

- Whether or not data can be shared between different hosts.
- Example: User home directories may be shareable, device lock files are not.

Variable vs. static:

- Whether or not data can change without a sysadmin's help.
- Example: Binaries and documentation don't change without help.

## File Hierarchy Standard (FHS)

The [FHS](https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf) specifies main directories and their purposes. Most Linux distributions respect it, but likely none follow it exactly.

Administered by The Linux Foundation (and originally by the Free Standard Group).

| Directory | Purpose                                                                                                                                                                                                                                                                                                                              |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `/`       | Primary directory.                                                                                                                                                                                                                                                                                                                   |
| `/bin`    | Essential executables that must be available in single user mode.                                                                                                                                                                                                                                                                    |
| `/boot`   | Files needed to boot the system. Essential files are `vmlinuz` (compressed kernel) and `initramfs` (initial RAM filesystem mounted before the real root filesystem; sometimes called `initrd` for initial RAM disk).                                                                                                                 |
| `/dev`    | Device Nodes, used to interact with hardware and software devices. Modern distributions use the udev system, which creates nodes here only as needed when devices are found.                                                                                                                                                         |
| `/etc`    | System-wide config files and startup scripts.                                                                                                                                                                                                                                                                                        |
| `/home`   | User home directories, including personal settings and files.                                                                                                                                                                                                                                                                        |
| `/lib`    | Libraries required by executables in `/bin` and `/sbin`. Includes Linux kernel modules. Some distributions just symlink to `/usr/lib` and abandon the possibility of placing `/usr` on a separate partition mounted after boot.                                                                                                      |
| `/lib64`  | Like `/lib`, but containing 64-bit libraries for systems that can run both 32-bit and 64-bit programs.                                                                                                                                                                                                                               |
| `/media`  | Mount points for removable media such as CDs, DVDs, and USB sticks.                                                                                                                                                                                                                                                                  |
| `/mnt`    | Temporarily mounted filesystems. Historically also used for the kinds of files now suggested to be mounted under `/media`.                                                                                                                                                                                                           |
| `/opt`    | Optional application software packages. Designed for software that wishes to keep all or most files in one isolated place. Often used by proprietary or antisocial software.                                                                                                                                                         |
| `/proc`   | Virtual pseudo-filesystem giving information about the system and processes running on it. Can be used to alter system parameters. All information here resides in memory only.                                                                                                                                                      |
| `/run`    | Run-time variable data, containing information describing the system since it was booted. Generally information here resides in memory only.                                                                                                                                                                                         |
| `/sys`    | Like `/proc`, but similar to a device tree and is part of the Unified Device Model. Younger than `/proc` and has strict standards.                                                                                                                                                                                                   |
| `/root`   | Home directory for the root user.                                                                                                                                                                                                                                                                                                    |
| `/sbin `  | Essential system binaries in addition to those in `/bin`; used for booting, restoring, recovering, and/or repairing.                                                                                                                                                                                                                 |
| `/srv`    | Site-specific data served up by the system. Seldom used. Subdirectories are sometimes structured by protocol, for example: `/srv/ftp`.                                                                                                                                                                                               |
| `/tmp`    | Temporary files. Sometimes stored in RAM (especially on Fedora systems), and therefore large files should not be created here.                                                                                                                                                                                                       |
| `/usr`    | Multi-user applications, utilities, and data. Binaries that aren't essential enough to be in `/bin` go in `/usr/bin`. Can be thought of as a secodary hierarchy not needed for booting. Can be shared among hosts using the same system architecture across a network (assuming the distribution hasn't abandoned this possibility). |
| `/var`    | Variable data that changes during system operation.                                                                                                                                                                                                                                                                                  |

Some additional directories not included in the standard are `/misc` for miscellaneous data, and `/tftpboot` for booting using **tftp**.

## Demo: Main Consumers Of Storage

Use `du` to examine disk usage by each directory (excluding the memory-only `/proc` directory):

```
du -shxc --exclude=proc /*
```

Pick a directory and describe files therein:

```
file /usr/lib/vlc/*
```

## Exercise: Tour `/proc`

View files in `/proc`:

```
ls -F /proc
```

- Numbers are currently running process IDs.

View constantly updated `uptime` file:

```
cat /proc/uptime
cat /proc/uptime
```

Have a look at memory info:

```
less /proc/meminfo
```

Inspect a running process:

```
less /proc/822/status
less /proc/822/environ
ls -l /proc/822/cwd
```
