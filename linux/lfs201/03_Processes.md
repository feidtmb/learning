# Processes

- [What Is A Program](#what-is-a-program)
- [What Is A Process](#what-is-a-process)
- [Process Attributes](#process-attributes)
- [Process Resource Isolation](#process-resource-isolation)
- [Controlling Processes With `ulimit`](#controlling-processes-with-ulimit)
- [Creating Processes](#creating-processes)
- [Creating Processes In A Command Shell](#creating-processes-in-a-command-shell)
- [Process States](#process-states)
- [Execution Modes](#execution-modes)
  - [User Mode](#user-mode)
  - [System (Kernel) Mode](#system-kernel-mode)
- [Daemons](#daemons)
- [Using `nice` To Set Priorities](#using-nice-to-set-priorities)
- [Modifying The Nice Value](#modifying-the-nice-value)
- [Static And Shared Libraries](#static-and-shared-libraries)
- [Shared Library Versions](#shared-library-versions)
- [Finding Shared Libraries](#finding-shared-libraries)
- [Examining System V IPC Activity](#examining-system-v-ipc-activity)

## What Is A Program

A **program** is a set of instructions plus any internal data used while carrying out the instructions. Programs may use external data. Programs may consist of machine level instructions run directly by a CPU, or a list of commands interpreted by another program. Sometimes the latter are referred to as scripts.

## What Is A Process

A **process** is an instance of a program in execution. Every process has a **pid** (Process ID), a **ppid** (Parent Process ID), and a **pgid** (Process Group ID).

Every process also has program code, data, variables, file descriptors, and an environment.

Processes are controlled by **scheduling**, and only the kernel has the right to schedule processes.

For historical reasons, some systems limit the largest PID to a 16-bit number (32768). This may be changed by modifying `/proc/sys/kernel/pid_max`. Once the max PID is reached, new processes will start again at PID 300.

Programs with multiple threads have one process per thread.

## Process Attributes

All processes have these attributes:

- Program being executed
- Context (state)
- Permissions
- Resources

Capturing and restoring snapshots of a process's context (**context switching**) is how the kernel handles switching between which process is executing at any given moment. The context of a process includes the state of its CPU registers, where it's executing in the program, what's in the process' memory, and other information.

A process's permissions are based on either the user who called the process to execute, or the user who owns the process's program file. The latter are referred to as **setuid** programs, and are marked with an "s" execute bit. An example of this is the `passwd` command, which is owned by root so that it can update write-restricted files despite allowing any user to execute it. These sorts of programs can obviously create security concerns.

Resources are things like allocated memory and file handles.

## Process Resource Isolation

Each process is isolated in its own user space to protect it from other processes.

Processes do not directly access hardware; and instead use system calls to indirectly access hardware. System calls are the fundamental interface between an application and the kernel.

## Controlling Processes With `ulimit`

`ulimit` is a built-in bash command that displays or sets resource limits associated with processes running under a shell. The `/etc/security/limits.conf` file may be modified to make changes that affect all logged-in users (and not just processes executed from the current shell).

There are two kinds of limits, hard and soft. The hard value can only be set by root, and the user-modifiable soft limit can not exceed it.

Documentation for `ulimit` can be found in the bash shell's man page: `man bash`.

## Creating Processes

New processes are created via **forking**, which is when the parent process spins off a child process.

Often fork is followed by exec, where the parent process terminates and the child process inherits the parent's PID. This is called **fork and exec**.

Older UNIX systems often used a program called spawn, which is similar to fork and exec.

Internal kernel processes handle maintenance work, such as flushing buffers to disk and ensuring that CPU load is balanced evenly. These processes usually run as long as the system is running, sleeping except when they have something to do.

External kernel processes run in user space like normal applications, but are started by the kernel. There are very few of these and they are usually short lived.

## Creating Processes In A Command Shell

The following happens when a user executes a command in a shell:

1. A new process is forked (created) from the user's shell.
2. A wait system call puts the parent process (the shell) to sleep.
3. The command is loaded onto the child process's space via the `exec` system call.
4. The command completes executing, and the child process dies via the exit system call.
5. The parent process is re-awakened by the death of the child process and proceeds to issue a new shell prompt.

If a command is issued for background processing (by adding an ampersand `&` at the end of the command line), the shell skips the wait request and is free to issue a new shell prompt immediately, allowing the background process to execute in parallel. Otherwise, for foreground requests, the shell waits until the child process has completed or is stopped via a signal.

Some shell commands (such as `echo` and `kill`) are built into the shell itself, and do not involve loading of program files. For these commands, neither a fork nor an exec is issued for the execution.

## Process States

The scheduler manages process states, and the state is reported by the process listing.

There are four main process states:

- Running: Either currently executing on a CPU or CPU core or sitting in the **run queue**.
- Sleeping: Waiting on a request (usually I/O) that it has made.
- Stopped: Suspended, usually by a debugger or the user hitting CTRL+Z.
- Zombie: Terminated, but no other process (usually the parent) has inquired about its exit state. The only resources these processes retain are their exit state and entry in the process table.

If the parent of any process dies, the process is adopted by `init` (PID = 1) or `kthreadd` (PID = 2).

## Execution Modes

At any given time, a process (or any particular thread of a multi-threaded process) may be executing in either user mode or system mode (often called kernel mode by kernel developers).

What instructions can be executed depends on the mode and is enforced at the hardware level.

The mode is not a state of the system; it is a state of the processor. In a multi-core or multi-CPU system, each unit can be in its own individual state.

In Intel terminology, user mode is termed Ring 3, and system mode is termed Ring 0.

### User Mode

Except when executing a system call, processes execute in user mode.

Each process executing in user mode has its own memory space, parts of which may be shared with other processes. Except for the shared memory segments, a user process is not able to read or write into or from the memory space of any other process.

Even a process with root user permission runs in user mode, except when jumping into a system call.

### System (Kernel) Mode

In system mode, the CPU has full access to all hardware on the system. If an application needs access to these resources, it must issue a system call, which causes a context switch from user mode to kernel mode. This procedure must be followed when doing things like reading and writing from files, and creating new processes.

Application code never runs in kernel mode, only the system call itself (which is kernel code).

## Daemons

A daemon process is a background process with the sole purpose of providing some specific service to users of the system. Daemon names often (but not always) end with "d".

## Using `nice` To Set Priorities

Process priority can be controlled through the `nice` and `renice` commands. The higher the niceness value, the lower the process priority (since nice processes yield system resources to other processes).

The `nice` command sets the niceness of a new process (`renice` is used for already running processes).

The niceness value can range from -20 to +19.

A process's niceness value can be inspected using `ps -l` and looking at the `NI` column.

## Modifying The Nice Value

`renice` is used to adjust the nice value of an already running process.

By default only a superuser can decrease niceness, even in cases where it was increased by a normal user. It is however possible to give normal users this ability within a predetermined range by editing `/etc/security/limits.conf`.

## Static And Shared Libraries

Static library code is inserted into a program at compile time.

Shared library code is loaded into a program at run time. Shared Libraries are also called Dynamic Link Libraries (DLLs).

Using shared libraries is more efficient; resulting in reduced memory usage, executable sizes, load times.

## Shared Library Versions

Shared libraries need to be carefully versioned, to avoid introducing breaking changes to a program that is not prepared to handle them.

Programs usually opt to use a specific major version and the latest minor version available.

Shared libraries have the extension `.so`. Typically, the full name is something like `libc.so.N`, where `N` is a major version number.

## Finding Shared Libraries

`ldd` can be used to figure out the shared libraries that an executable requires.

`ldconfig` is used to create shared library links and cache. It's normally run at boot, and uses `/etc/ld.so.conf` in addition to other sources, which list the directories that will be searched for shared libraries.

In addition to searching the database built by `ldconfig`, the linker will first search any directories specified in the environment variable `LD_LIBRARY_PATH`, which is a colon separated list of directories similar to `PATH`.

## Examining System V IPC Activity

Modern programs usually use POSIX IPC for Inter Process Communication, but the older System V IPC method is still in use in the wild.

It involves three mechanisms:

1. Shared Memory Segments
2. Semaphores
3. Message Queues

The command `ipcs` can be used to get an overall summary of System V IPC activity.

- Shared memory segments with a key of 0 (`IPC_PRIVATE`) are only shared between processes in a parent/child relationship.
- Segments with status `dest` are marked for destruction when there are no further attachments. Segments lacking this status and with no attachments might persist forever leaking memory.

`ipcs -p` can be used to see the process IDs that created and last attached to shared memory segments. We can then use `ps aux` and `grep -e` to see the names of the programs associated with the PIDs: `ps aux | grep -e <PID_1> -e <PID_2>`.
