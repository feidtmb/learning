# Package Management Systems

- [Why Use Packages?](#why-use-packages)
- [Software Packaging Concepts](#software-packaging-concepts)
- [Package Types](#package-types)
- [Available Package Management Systems](#available-package-management-systems)
- [Packaging Tool Levels And Varieties](#packaging-tool-levels-and-varieties)
- [Package Sources](#package-sources)
- [Creating Software Packages](#creating-software-packages)
- [Revision Control Systems](#revision-control-systems)
- [The Linux Kernel And The Birth Of `git`](#the-linux-kernel-and-the-birth-of-git)
- [How `git` Works](#how-git-works)

## Why Use Packages?

Software package management systems are widely seen as one of the biggest advancements Linux brought to enterprise IT environments. By keeping track of files and metadata in an automated, predictable and reliable way, system administrators can use package management systems to make their installation processes scale to thousands of systems without requiring manual work on each individual system.

Features include:

- Automation: No need for manual installs and upgrades.
- Scalability: Install packages on one system, or 10,000 systems.
- Repeatability and predictability.
- Security and auditing.

## Software Packaging Concepts

Package management systems do the following:

- Gather and compress associated software files into a single package (archive), which may require other packages to be installed first.
- Allow for easy software installation or removal.​
- Can verify file integrity via an internal database.​
- Can authenticate the origin of packages.​
- Facilitate upgrades.​
- Group packages by logical features.​
- Manage dependencies between packages.

A given package may contain executable files, data files, documentation, installation scripts, and configuration files. Also included are metadata attributes such as version numbers, checksums, vendor information, dependencies, descriptions, and so on.

Upon installation, all that information is stored locally into an internal database, which can be conveniently queried for version status and update information.

## Package Types

- Binary Packages
  - Architecture dependent executables and libraries ready for deployment.
- Source Packages
  - Used to generate binary packages for multiple architectures.
- Architecure-Independent
  - Scripts and associated files, including configuration and documentation files.
- Meta-Packages
  - Groups of associated packages that are together needed to install a large subsystem, such as a desktop environment or office suite.

Binary packages are the most common. It should always be possible to rebuild a binary package from the source package, and the source package should always be available from the vendor (though is not normally installed by default).

## Available Package Management Systems

Two common package management systems are `rpm` (Red Hat Package Manager) and `dpkg` (Debian Package -- which is used internally by `apt` commands). Other common ones include `portage`/`emerge` used by Gentoo, and `pacman` used by Arch.

When the above aren't in use, sometimes packages are just supplied as tarballs without any management or clean removal strategies. Slackware, one of the oldest Linux distributions, uses this approach.

## Packaging Tool Levels And Varieties

Low level utilities include `rpm` and `dpkg`, and are responsible for the following:

- Install or remove a single package.
- List packages.
- Raise errors or warnings related to dependencies, but don't otherwise handle them.

High level utilities include `apt` and `dnf` (formerly `yum`), and are responsible for solving dependency problems while using lower level utilities.

## Package Sources

Every Linux distribution has one or more package repositories, where system utilities go to obtain software and to update with new versions. It's the job of the distribution to make sure all packages in the repositories are compatible with each other.

There are also external repositories which can be added to the standard distribution-supported set. Sometimes these are closely associated with the distribution, and only rarely produce significant problems. However, some external repositories are not very well constructed or maintained, and can lead to various problems commonly including issues with dependencies.

## Creating Software Packages

Almost every Linux distribution has some mechanism for creating custom software packages.

Tasks needed to install the new custom software and/or remove the old custom software should be configured to happen during installation, and include such things as:

- Creating needed symbolic links
- Creating directories as needed
- Setting permissions
- ...

Some package formats include:

- RPM for Red Hat and SUSE-based systems
- DEB for Debian for Debian-based systems
- TGZ for Slackware
- APK for Android 

## Revision Control Systems

(`git`)

## The Linux Kernel And The Birth Of `git`

The Linux kernel development system has special needs in that it is widely distributed throughout the world, with thousands of developers involved. Furthermore it is all done very publicly, under the GPL license.

For a long time, there was no real source revision control system. Then, major kernel developers went over to the use of BitKeeper, a commercial project which granted a restricted use license for Linux kernel development.

However, in a very public dispute over licensing restrictions in the spring of 2005, the free use of BitKeeper became unavailable for Linux kernel development.

The response was the development of `git`, whose original author was Linus Torvalds.

## How `git` Works

The basic units `git` works with are not files. It has two important data structures: an **object database** and a **directory cache**.

The object database contains objects of three varieties:

- Blobs
  - Chunks of binary data containing file contents.
- Trees
  - Sets of blobs including file names and attributes, giving the directory structure.
- Commits
  - Changesets describing tree snapshots.

The directory cache captures the state of the directory tree.
