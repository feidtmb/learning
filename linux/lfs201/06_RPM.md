# RPM

- [Advantages Of Using RPM](#advantages-of-using-rpm)
- [Package File Names](#package-file-names)
- [RPM Database And Helper Programs](#rpm-database-and-helper-programs)
- [Queries](#queries)
- [Verifying Packages](#verifying-packages)
- [Installing Packages](#installing-packages)
- [Uninstalling Packages](#uninstalling-packages)
- [Updating Packages](#updating-packages)
- [Freshening Packages](#freshening-packages)
- [Upgrading The Linux Kernel](#upgrading-the-linux-kernel)
- [Using `rpm2archive` And `rpm2cpio`](#using-rpm2archive-and-rpm2cpio)

## Advantages Of Using RPM

All files related to a specific task or a subsystem are packaged into a single RPM package file, which also contains information about how and where to install and uninstall the files.

Using RPM packages makes it is easy to determine which package a particular file comes from, which version of the package is installed, and whether it was installed correctly. It's also easy to remove complete packages. Furthermore, RPM distinguishes documentation files from the rest of a package, allowing you to choose whether to install documentation on a system.

As far as software development goes, RPM allows builders to keep the changes necessary for building on Linux separate from the original source code.

## Package File Names

The standard naming format for a binary RPM package is:

`name-version-release.distro.architecture.rpm`

For example: `sed-4.5-2.e18.x86_64.rpm`

The standard naming format for a source RPM package is:

`name-version-release.distro.src.rpm`

For example: `sed-4.5-2.e18.src.rpm`

Note that the distro field often actually specifies the repository that the package came from, as a given installation may use a number of different package repositories.

## RPM Database And Helper Programs

`/var/lib/rpm` is the default system directory which holds RPM database files, which should only be updated through the use of the `rpm` program.

An alternative database directory can be specified with the `--dbpath` option to the rpm program (to examine an RPM database copied from another system, for example).

The `--rebuilddb` option repairs database indices from the installed package headers.

Helper programs and scripts used by RPM reside in `/usr/lib/rpm`. There are usually a good number of these.

You can create an `rpmrc` file to specify default settings. By default, `rpm` looks for the following in order:

- `/usr/lib/rpm/rpmrc`
- `/etc/rpmrc`
- `~/.rpmrc`

An alternative `rpmrc` file can be specified using the `--rcfile` option.

## Queries

All `rpm` inquiries include the `-q` option, which can be combined with other query options:

- `-f`: find which package a file came from
- `-l`: list contents of a specific package
- `-a`: all packages installed on the system
- `-i`: information about the package
- `-p`: run the query against a package file instead of the package database

Examples:

| Task                                                                                 | Command                           |
| ------------------------------------------------------------------------------------ | --------------------------------- |
| Which version of a package is installed?                                             | `rpm -q bash`                     |
| Which package did this file come from?                                               | `rpm -qf /bin/bash`               |
| What files were installed by this package?                                           | `rpm -ql bash`                    |
| Show information about this package.                                                 | `rpm -qi bash`                    |
| Show information about this package from the package file, not the package database. | `rpm -qip foo-1.0.0-1.noarch.rpm` |
| List all installed packages on this system.                                          | `rpm -qa`                         |

A couple of other useful options are `--requires` and `--whatprovides`. The `--requires` option will return a list of prerequisites for a package, while the `--whatprovides` option will show what installed package provides a particular requisite package. For example:

```
rpm -q --requires bash
rpm -qp --requires foo-1.0.0-1.noarch.rpm
rpm -q --whatprovides libc.so.6
```

## Verifying Packages

The `-V` option verifies a package against the system’s RPM database, and `-Va` verifies all packages.

If there's no problem, there's no output. Otherwise output is comprised of one character per attribute checked, with a `.` indicating no problems with the attribute and a `?` indicating the attribute could not be verified. Any other character indicates a failed test. Attribute characters are:

- `S`: filesize
- `M`: mode (permissions and file type)
- `5`: MD5 sum
- `D`: device major/minor number
- `L`: readLink path
- `U`: user ownership
- `G`: group ownership
- `T`: mTime

## Installing Packages

Use `-i` to install a package. Other useful options include `v` for verbose and `h` to print hash marks denoting progress.

Example: `sudo rpm -ivh bash-4.4.19-12.el8_0.x86_64`

Tasks completed by RPM during installation:

1. dependency checks
2. conflict checks
3. execute pre-install commands (defined by developer)
4. deal with configuration files (for example, by changing the suffix of an existing one to `.rpmsave`)
5. unpack and install files to the correct lo ations and with the correct attributes (permission, ownership, modification time, ...)
6. execute post-install commands
7. update system RPM database

## Uninstalling Packages

Use `-e` to uninstall (erase) a package by name (not by file name). The `--test` option can be used to check if the uninstall would succeed without actually performing it, and the `-vv` option can be used to get more information.

Be aware that `rpm` can uninstall itself if you tell it to, and fixing that often requires an OS reinstall or use of a rescue environment.

## Updating Packages

Use `-U` to upgrade a list of packages by file name. Listed packages will be installed if not already present. The `--oldpackage` option can be used to downgrade.

Different versions of the same package may be installed if the versions don't contain the same files. This typically only happens for kernel and library packages from alternative architectures.

## Freshening Packages

Use `-F` to "freshen" packages by file name. Older versions are upgraded, matching ones ignored, and uninstalled packages are ignored.

This option is useful for applying a bunch of patches while avoiding installing new packages.

## Upgrading The Linux Kernel

It's recommended to only install new versions of the kernel, never upgrade, since upgrading will remove the old version; meaning if there's any problem you won't be able to reboot into the old kernel.

A reboot is required when the kernel is upgraded, and (unless configured otherwise) the GRUB configuration file will be automatically updated to boot the new kernel.

Installing a new kernel on an RHEL system can be done with the following:

`sudo rpm -i kernel-{version}.{arch}.rpm`

## Using `rpm2archive` And `rpm2cpio`

`rpm2archive` converts RPM package files to tar archives. For example: `rpm2archive bash-xxx.rpm` creates `bash-xxx.rpm.tgz`.

`rpm2cpio` is an older utility that converts package files to cpio archives or extracts files from the package file. For example: `rpm2cpio bash-xxx.rpm > bash.cpio` or `rpm2cpio bash-xxx.rpm | cpio --extract --make-directories`.

If the goal is to list the files in a package, use `rpm -qilp package.rpm` instead.
