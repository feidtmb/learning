# DPKG

- [DPKG Essentials](#dpkg-essentials)
- [Package File Names And Source](#package-file-names-and-source)
- [DPKG Queries](#dpkg-queries)
- [Installing/Upgrading/Uninstalling Packages With `dpkg`](#installingupgradinguninstalling-packages-with-dpkg)

## DPKG Essentials

`dpkg` is used by Debian and Debian derived distributions. Like `rpm`, it's designed to handle installing and removing packages, and only knows about packages on the system or given via commands.

Package files use the `.deb` extension, and the database is in `/var/lib/dpkg`.

## Package File Names And Source

Naming format for a binary package is `{name}_{version}-{revision_number}_{architecture}.deb`. For example, `logrotate_3.14.0-4_amd64.deb`.

A source package consists of at least three files:

- An upstream tarball ending with `.tar.gz`. This is the source as it comes from the package maintainers.
- A description file ending with `.dsc`, containing the package name and other metadata.
- A second tarball ending with `.debian.tar.gz` or `.diff.gz`. This contains patches to the upstream source and any additional files created for the package.

## DPKG Queries

Examples:

- List all packages installed: `dpkg -l`
- List files installed in the wget package: `dpkg -L wget`
- Show information about an installed package: `dpkg -s wget`
- Show information about a package file: `dpkg -I webfs_1.21+ds1-8_amd64.deb`
- List files in a package file: `dpkg -c webfs_1.21+ds1-8_amd64.deb`
- Show what package owns the /etc/init/networking.conf file: `dpkg -S /etc/init/networking.conf`
- Verify the installed package's integrity: `dpkg -V package`

## Installing/Upgrading/Uninstalling Packages With `dpkg`

Install or upgrade: `dpkg -i <package>.deb`

Remove except for config: `dpkg -r <package>`

Remove all including config (purge): `dpkg -P <package>`
