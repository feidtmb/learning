# DNF And YUM

- [Package Installers](#package-installers)
- [What Is `dnf`?](#what-is-dnf)
- [What is `yum`?](#what-is-yum)
- [Queries](#queries)
- [Installing / Removing / Upgrading Packages](#installing--removing--upgrading-packages)
- [Additional `dnf` Commands](#additional-dnf-commands)

## Package Installers

`rpm` and `dpkg` are lower-level package managers, while `dnf`, `yum`, `apt`, and `zypper` are some examples of higher-level package managers.

Higher-level package managers:

- Use both local and remote repositories as sources to install and update binaries and source software packages.
- Can automate software package install, upgrade, and removal.
- Resolve dependencies automatically.

Software repositories are provided by distributions as well as independent providers.

## What Is `dnf`?

`dnf` is a frontend to `rpm`, and adds the ability to retrieve packages from remote repositories in order to resolve dependencies. Config files for software repositories are in `/etc/yum.repos.d` and have the `.repo` extension.

Individual repos may be enabled or disabled either via these config files, or by using the `--disablerepo` and `--enablerepo` options.

`dnf` uses a cache, which may be cleared with the `dnf clean` command.

## What is `yum`?

`yum` is the predecessor to `dnf`, and was in use before the RHEL 7 to 8 transition.

`dnf` is backwards compatible and takes a subset of common `yum` commands, making it easy for those familiar with `yum` to learn.

## Queries

`dnf search <keyword>`

- Search for package with keyword

`dnf info <package-name>`

- Display info about package

`dnf list [installed | available | updates ]`

- List packages installed, available, or with updates

`dnf grouplist`

- Show information about package groups

`dnf groupinfo <packagegroup>`

- Show information about a single package grou

`dnf provides </path/to/file>`

- Show package providing file (command requires at least one `/` in file path)

## Installing / Removing / Upgrading Packages

Install package (and automatically resolve and install dependencies): `dnf install <package>`

Install package from local file: `dnf localinstall <package.rpm>`

Install software group (and automatically handle dependencies): `dnf groupinstall <'group-name'>`

Remove package from system: `dnf remove <package>`

Update all packages or a single package: `dnf update [package]`

When installing or updating packages, config files are renamed `.rpmsave` or `.rpmnew` depending on whether or not the old file will continue to work. (If it will, the new config file will be named `.rpmnew`, otherwise the old will be named `.rpmsave`.)

## Additional `dnf` Commands

`dnf list "dnf-plugin*"`

- List dnf plugins

`dnf repolist`

- List enabled repositories

`dnf shell [file]`

- Provide a shell to run multiple `dnf` commands, or execute the commands in a file

`dnf install --downloadonly <package>`

- Download but don't install the package (stored in `/var/cache/dnf`)

`dnf history`

- View history of `dnf` commands on the system. Can also undo or redo previous commands using options.

`dnf clean [packages | metadata | expire-cache | rpmdb | plugins | all ]`

- Clean up `/var/cache/dnf`.
