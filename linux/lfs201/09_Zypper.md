# Zypper

- [What Is `zypper`?](#what-is-zypper)
- [`zypper` Queries](#zypper-queries)
- [Installing / Removing / Upgrading Packages with `zypper`](#installing--removing--upgrading-packages-with-zypper)
- [Additional `zypper` Commands](#additional-zypper-commands)

## What Is `zypper`?

`zypper` is the package manager for SUSE Linux and openSUSE. It's very similar to `dnf` and works with `rpm` packages.

## `zypper` Queries

`zypper list-updates`

- List available updates

`zypper repos`

- List available repositories

`zypper search <keyword>`

- Search repositories for keyword

`zypper info <package>`

- List info about package

`zypper search --provides /path/to/file`

- Show what package provides file

## Installing / Removing / Upgrading Packages with `zypper`

`zypper install <package>`

- Install or update package

`zypper --non-interactive install <package>`

- Install or update package without confirmation prompts (useful for scripts)

`zypper update`

- Update all packages

`zypper remove <package>`

- Remove package (and any package that requires it)

## Additional `zypper` Commands

`zypper shell`

- Provides a shell for running additional commands, without needing to re-read all the databases with each.

`zypper addrepo <URI> <alias>`

- Add a new repository

`zypper removerepo <alias>`

- Remove a repository

`zypper clean [--all]`

- Clean up `/var/cache/zypp`

