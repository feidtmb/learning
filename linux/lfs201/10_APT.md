# APT

- [What Is APT?](#what-is-apt)
- [Queries](#queries)
- [Installing / Removing / Upgrading Packages With `apt`](#installing--removing--upgrading-packages-with-apt)
- [Cleaning Up](#cleaning-up)

## What Is APT?

APT (Advanced Packaging Tool) is the package manager for Debian-based systems. It uses `dpkg` and plays the same role as `dnf`.

It works with `.deb` package files.

## Queries

Queries are done using the `apt-cache` or `apt-file` utilities.

`apt-file` may need to be installed and its database udpated:

```
apt-get install apt-file
apt-file update
```

Following are example queries:

`apt-cache search <package>`

- Search repositories for package

`apt-cache show <package>`

- Show basic information about package

`apt-cache showpkg <package>`

- Show detailed information about package

`apt-cache depends <package>`

- List all dependent packages

`apt-file search <file>`

- Search repositories for file

`apt-file list <package>`

- List all files in package

## Installing / Removing / Upgrading Packages With `apt`

`apt-get install [package]`

- Install or update package

`apt-get remove [package]`

- Remove package (use `--purge` to remove config files also)

`apt-get update`

- Synchronize package index files with their sources. Uses location(s) specified in `/etc/apt/sources.list`

`apt-get upgrade`

- Apply updates to installed packages. Always run `update` before `upgrade`.

## Cleaning Up

This command gets rid of any packages not needed anymore, such as older Linux kernel versions:

`apt-get autoremove`

- Remove packages that aren't needed anymore, like older Linux kernel versions.

`apt-get clean`

- Remove cache files and archived package files that were installed. Can save a lot of space.
