# System Monitoring

- [Monitoring Tools](#monitoring-tools)
  - [Process and Load Monitoring Utilities](#process-and-load-monitoring-utilities)
  - [Memory Monitoring Utilities](#memory-monitoring-utilities)
  - [I/O Monitoring Utilities](#io-monitoring-utilities)
  - [Network Monitoring Utilities](#network-monitoring-utilities)
- [`sar`](#sar)
- [Log Files](#log-files)
- [Using `stress` Or `stress-ng`](#using-stress-or-stress-ng)

## Monitoring Tools

Most monitoring tools make heavy use of `/proc` and secondarily `/sys`.

### Process and Load Monitoring Utilities

| Utility  | Purpose                                                  | Package            |
| -------- | -------------------------------------------------------- | ------------------ |
| top      | Process activity, dynamically updated                    | procps             |
| uptime   | How long the system is running and the average load      | procps             |
| ps       | Detailed information about processes                     | procps             |
| pstree   | A tree of processes and their connections                | psmisc (or pstree) |
| mpstat   | Multiple processor usage                                 | sysstat            |
| iostat   | CPU utilization and I/O statistics                       | sysstat            |
| sar      | Display and collect information about system activity    | sysstat            |
| numastat | Information about NUMA (Non-Uniform Memory Architecture) | numactl            |
| strace   | Information about all system calls a process makes       | strace             |

### Memory Monitoring Utilities

| Utility | Purpose                                                               | Package |
| ------- | --------------------------------------------------------------------- | ------- |
| free    | Brief summary of memory usage                                         | procps  |
| vmstat  | Detailed virtual memory statistics and block I/O, dynamically updated | procps  |
| pmap    | Process memory map                                                    | procps  |

### I/O Monitoring Utilities

| Utility | Purpose                                                               | Package |
| ------- | --------------------------------------------------------------------- | ------- |
| iostat  | CPU utilization and I/O statistics                                    | sysstat |
| sar     | Display and collect information about system activity                 | sysstat |
| vmstat  | Detailed virtual memory statistics and block I/O, dynamically updated | procps  |

### Network Monitoring Utilities

| Utility   | Purpose                                          | Package   |
| --------- | ------------------------------------------------ | --------- |
| netstat   | Detailed networking statistics                   | netstat   |
| iptraf    | Gather information on network interfaces         | iptraf    |
| tcpdump   | Detailed analysis of network packets and traffic | tcpdump   |
| wireshark | Detailed network traffic analysis                | wireshark |

 ## `sar`

`sar` stands for Systems Activity Reporter, and is an all-purpose tool for gathering and presenting activity and performance data in a human readable format.

Under the hood, `sar` uses `sadc` (system activity data collector), which stores information in `/var/log/sa` with an adjustable frequency (daily by default). Periodic data collection is usually started as a cron job stored in `/etc/cron.d/sysstat`.

The actual `sar` utility simply reads the collected data and presents it in report format.

Example: `sar [options] [interval] [count]`

- Interval (in seconds) and count indicate how many times the report should be produced and over what period. The default count is one, and the default report is on CPU usage.

| Option | Meaning                                                                     |
| ------ | --------------------------------------------------------------------------- |
| -A     | Almost all information                                                      |
| -b     | I/O and transfer rate statistics (similar to iostat)                        |
| -B     | Paging statistics including page faults                                     |
| -d     | Block device activity (similar to iostat -x)                                |
| -n     | Network statistics                                                          |
| -P     | Per CPU statistics (as in sar -P ALL 3)                                     |
| -q     | Queue lengths (run queue, processes and threads)                            |
| -r     | Memory utilization statistics                                               |
| -S     | Swap utilization statistics                                                 |
| -u     | CPU utilization (default)                                                   |
| -v     | Statistics about inodes and files and file handles                          |
| -w     | Context switching statistics                                                |
| -W     | Swapping statistics, pages in and out per second                            |
| -f     | Extract information from specified file, created by the -o option           |
| -o     | Save readings in the file specified, to be read in later with the -f option |

## Log Files

Linux log files are stored in `/var/log`.

Logs are handled by `syslogd`, or `rsyslogd` on more modern systems. For `systemd`-based systems, `journalctl` may be used instead, but it usually cooperates with `syslogd`.

New messages may be viewed as they arrive using `tail -f /var/log/messages`. To view only kernel-related messages: `dmesg -w`.

Common important log files in `var/log`:

| File                   | Purpose                                                                   |
| ---------------------- | ------------------------------------------------------------------------- |
| `boot.log`             | ​System boot messages                                                     |
| `dmesg`                | ​Kernel messages saved after boot (excludes messages in the dmesg buffer) |
| `messages` or `syslog` | All important system messages                                             |
| `secure`               | Security-related messages                                                 |

The `logrotate` program prevents log files from growing too large by replacing log files periodically. By default four previous copies are retained. Config for this program is at `/etc/logrotate.conf`.

## Using `stress` Or `stress-ng`

`stress` is a C program for generating various workloads on a system for stress testing. `stress-ng` is an enhanced and actively maintained version of the program.

The example command `stress-ng -c 8 -i 4 -m 6 -t 20s` will do the following:

- Fork off 8 CPU-intensive processes.
- Fork off 4 I/O-intensive processes.
- Fork off 6 memory-intensive processes, each allocating 256 MB by default.
- Run for 20 seconds.
