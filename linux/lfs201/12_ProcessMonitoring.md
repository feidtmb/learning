# Process Monitoring

- [Process Monitoring Tools](#process-monitoring-tools)
  - [Process and Load Monitoring Tools](#process-and-load-monitoring-tools)
- [Viewing Process States With `ps`](#viewing-process-states-with-ps)
- [Customizing `ps` Output](#customizing-ps-output)
- [Using `pstree`](#using-pstree)
- [`top`](#top)
- [More On `/proc`](#more-on-proc)

## Process Monitoring Tools

Most process monitoring tools use `/proc`.

### Process and Load Monitoring Tools

| Tool       | Purpose                                                  |
| ---------- | -------------------------------------------------------- |
| `top`      | Process activity, dynamically updated                    |
| `uptime`   | How long the system is running and the average load      |
| `ps`       | Detailed information about processes                     |
| `pstree`   | A tree of processes and their connections                |
| `mpstat`   | Multiple processor usage                                 |
| `iostat`   | CPU utilization and I/O statistics                       |
| `sar`      | Display and collect information about system activity    |
| `numastat` | Information about NUMA (Non-Uniform Memory Architecture) |
| `strace`   | Information about all system calls a process makes       |

## Viewing Process States With `ps`

`ps` is a very common method for gathering process data.

It's commonly executed with the following options, which vary heavily due to how long `ps` has existed:

```
ps aux
ps -elf
ps -eL
ps -C "<program_name>"
```

Options to `ps` come in a variety of forms that can be broken down based on the OS variant from which they originate:

- UNIX options: preceded by `-` and may be grouped.
- BSD options: not preceded by `-` and may be grouped.
- GNU long options: preceded by `--`.

## Customizing `ps` Output

The `-o` option allows specifying a comma-separated list of fields for use in the output. Field names are:

- `pid`: Process ID number
- `uid`: User ID number
- `cmd`: Command with all arguments
- `cputime`: Cumulative CPU time
- `pmem`: Ratio of the process's memory to the physical memory on the machine, expressed as a percentage.

## Using `pstree`

`pstree` visually describes process ancestry and multi-threaded applications.

A common command is `pstree -aAp <pid>`.

Common options are `-p` to show process IDs, and `-H <pid>` to highlight a process and its ancestors.

## `top`

`top` is often used to find processes with the highest CPU usage, and that's how processes are sorted by default. The display refreshes regularly until interrupted (CTRL+C).

May also be used to signal processes by pressing the "k" key, unless run in secure mode (`top s`).

`top` is very old and has various other such features. Pressing the "h" key will list key bindings.

## More On `/proc`

Remember that `/proc` is an interface to kernel data structures; and contains a subdirectory for each process named by process ID, as well as other information.

