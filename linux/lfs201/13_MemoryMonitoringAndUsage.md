# Memory Monitoring And Usage

- [Memory Monitoring](#memory-monitoring)
  - [Memory Monitoring Utilities](#memory-monitoring-utilities)
- [`/proc/meminfo`](#procmeminfo)
  - [`/proc/meminfo` Entries](#procmeminfo-entries)
- [`/proc/sys/vm`](#procsysvm)
  - [`/proc/sys/vm` Entries](#procsysvm-entries)
- [`vmstat`](#vmstat)
- [OOM-Killer](#oom-killer)

## Memory Monitoring

Tuning memory can be complex, and it's important to note that memory usage and I/O throughput are related, since in most cases the majority of memory is used to cache file contents.

### Memory Monitoring Utilities

| Utility  | Purpose                                                                | Package  |
| -------- | ---------------------------------------------------------------------- | -------- |
| `free`   | Brief summary of memory usage                                          | `procps` |
| `vmstat` | Detailed virtual memory statistics and b lock I/O, dynamically updated | `procps` |
| `pmap`   | Process memory map                                                     | `procps` |

## `/proc/meminfo`

`/proc/meminfo` is a pseudofile with tons of information about how memory is being used.

### `/proc/meminfo` Entries

Exact entries vary, but these are common:

| Entry           | Meaning                                                       |
| --------------- | ------------------------------------------------------------- |
| MemTotal        | Total usable RAM (physical minus some kernel reserved memory) |
| MemFree         | Free memory in both low and high zones                        |
| Buffers         | Memory used for temporary block I/O storage                   |
| Cached          | Page cache memory, mostly for file I/O                        |
| SwapCached      | Memory that was swapped back in but is still in the swap file |
| Active          | Recently used memory, not to be claimed first                 |
| Inactive        | Memory not recently used, more eligible for reclamation       |
| Active(anon)    | Active memory for anonymous pages                             |
| Inactive(anon)  | Inactive memory for anonymous pages                           |
| Active(file)    | Active memory for file-backed pages                           |
| Inactive(file)  | Inactive memory for file-backed pages                         |
| Unevictable     | Pages which can not be swapped out of memory or released      |
| Mlocked         | Pages which are locked in memory                              |
| SwapTotal       | Total swap space available                                    |
| SwapFree        | Swap space not being used                                     |
| Dirty           | Memory which needs to be written back to disk                 |
| Writeback       | Memory actively being written back to disk                    |
| AnonPages       | Non-file back pages in cache                                  |
| Mapped          | Memory mapped pages, such as libraries                        |
| Shmem           | Pages used for shared memory                                  |
| Slab            | Memory used in slabs                                          |
| SReclaimable    | Cached memory in slabs that can be reclaimed                  |
| SUnreclaim      | Memory in slabs that can't be reclaimed                       |
| KernelStack     | Memory used in kernel stack                                   |
| PageTables      | Memory being used by page table structures                    |
| Bounce          | Memory used for block device bounce buffers                   |
| WritebackTmp    | Memory used by FUSE filesystems for writeback buffers         |
| CommitLimit     | Total memory available to be used, including overcommission   |
| Committed_AS    | Total memory presently allocated, whether or not it is used   |
| VmallocTotal    | Total memory available in kernel for vmalloc allocations      |
| VmallocUsed     | Memory actually used by vmalloc allocations                   |
| VmallocChunk    | Largest possible contiguous vmalloc area                      |
| HugePages_Total | Total size of the huge page pool                              |
| HugePages_Free  | Huge pages that are not yet allocated                         |
| HugePages_Rsvd  | Huge pages that have been reserved, but not yet used          |
| HugePages_Surp  | Huge pages that are surplus, used for overcommission          |
| Hugepagesize    | Size of a huge page                                           |

## `/proc/sys/vm`

The virtual memory system can be tuned via the `/proc/sys/vm` directory. Exact entries in this directory depend on kernel version, and can be written to directly or modified using a `sysctl` utility.

Best practice when adjusting parameters is to change one thing at a time. Primary (inter-related) tasks are:

- Controlling flushing parameters (how many pages are allowed to be dirty and how often they are flushed out to disk).
- Controlling swap behavior (how much pages that reflect file contents are allowed to remain in memory, as opposed to those that need to be swapped out as they have no other backing store). 
- Controlling how much memory over-commission is allowed (since many programs never need the full amount of memory they request).

### `/proc/sys/vm` Entries

| Entry                      | Purpose                                                                                                                                                                                           |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| admin_reserve_kbytes       | Amount of free memory reserved for privileged users                                                                                                                                               |
| block_dump                 | Enables block I/O debugging                                                                                                                                                                       |
| compact_memory             | Turns on or off memory compaction (essentially defragmentation) when configured into the kernel                                                                                                   |
| dirty_background_bytes     | Dirty memory threshold that triggers writing uncommitted pages to disk                                                                                                                            |
| dirty_background_ratio     | Percentage of total pages at which kernel will start writing dirty data out to disk                                                                                                               |
| dirty_bytes                | The amount of dirty memory a process needs to initiate writing on its own                                                                                                                         |
| dirty_expire_centisecs     | When dirty data is old enough to be written out in hundredths of a second                                                                                                                         |
| dirty_ratio                | Percentage of pages at which a process writing will start writing out dirty data on its own                                                                                                       |
| dirty_writeback_centisecs  | Interval in which periodic writeback daemons wake up to flush. If set to zero, there is no automatic periodic writeback                                                                           |
| drop_caches                | Echo 1 to free page cache, 2 to free dentry and inode caches, 3 to free all. Note only clean cached pages are dropped; do sync first to flush dirty pages                                         |
| extfrag_threshold          | Controls when the kernel should compact memory                                                                                                                                                    |
| hugepages_treat_as_movable | Used to toggle how huge pages are treated                                                                                                                                                         |
| hugetlb_shm_group          | Sets a group ID that can be used for System V huge pages                                                                                                                                          |
| laptop_mode                | Can control a number of features to save power on laptops                                                                                                                                         |
| legacy_va_layout           | Use old layout (2.4 kernel) for how memory mappings are displayed                                                                                                                                 |
| lowmen_reserve_ratio       | Controls how much low memory is reserved for pages that can only be there; i.e., pages which can go in high memory instead will do so. Only important on 32-bit systems with high memory          |
| max_map_count              | Maximum number of memory mapped areas a process may have. The default is 64 K                                                                                                                     |
| min_free_kbytes            | Minimum free memory that must be reserved in each zone                                                                                                                                            |
| mmap_min_addr              | How much address space a user process cannot memory map. Used for security purposes, to avoid bugs where accidental kernel null dereferences can overwrite the first pages used in an application |
| nr_hugepages               | Minimum size of hugepage pool                                                                                                                                                                     |
| nr_pdflush_hugepages       | Maximum size of the hugepage pool = nr_hugepages *nr_overcommit_hugepages                                                                                                                         |
| nr_pdflush_threads         | Current number of pdflush threads; not writeable                                                                                                                                                  |
| oom_dump_tasks             | If enabled, dump information produced when oom-killer cuts in                                                                                                                                     |
| oom_kill_allocating_task   | If set, the oom-killer kills the task that triggered the out of memory situation, rather than trying to select the best one                                                                       |
| overcommit_kbytes          | One can set either overcommit_ratio or this entry, but not both                                                                                                                                   |
| overcommit_memory          | If 0, kernel estimates how much free memory is left when allocations are made. If 1, permits all allocations until memory actually does run out. If 2, prevents any overcommission                |
| overcommit_ratio           | If overcommit_memory = 2 memory commission can reach swap plus this percentage of RAM                                                                                                             |
| page-cluster               | Number of pages that can be written to swap at once, as a power of two. Default is 3 (which means 8 pages)                                                                                        |
| panic_on_oom               | Enable system to crash on an out of memory situation                                                                                                                                              |
| percpu_pagelist_fraction   | Fraction of pages allocated for each cpu in each zone for hot-pluggable CPU machines                                                                                                              |
| scan_unevictable_pages     | If written to, system will scan and try to move pages to try and make them reclaimable                                                                                                            |
| stat_interval              | How often vm statistics are updated (default 1 second) by vmstat                                                                                                                                  |
| swappiness                 | How aggressively should the kernel swap                                                                                                                                                           |
| user_reserve_kbytes        | If overcommit_memory is set to 2 this sets how low the user can draw memory resources                                                                                                             |
| vfs_cache_pressure         | How aggressively the kernel should reclaim memory used for inode and dentry cache. Default is 100; if 0 this memory is never reclaimed due to memory pressure                                     |

## `vmstat`

`vmstat` displays information about memory, paging, I/O, processor activity, and processes.

```
vmstat [options] [delay] [count]
```

Delay is in seconds, and if given causes the report to be repeated at that delay for count times, or forever if count is not given.

Regarding options:

- With no options, the first line shows averages since the last reboot, and following lines show activity during the delay period.
- `-S m` displays memory in MB instead of KB.
- `-a` option displays information about active and inactive memory.
- `-d` gives disk stats.
- `-p <partition>` gives stats related to a single disk partition.

## OOM-Killer

Linux permits overcommitment of memory for user processes, since most processes don't use the full amount of memory they request. Additionally, whenever a process is forked it receives a copy of its parent's memory; though because of Linux's Copy-On-Write technique this memory won't actually be allocated unless modified.

Overcommitment of memory can be modified or disabled by editting `/proc/sys/vm/overcommit_memory`:

- `0`: (default) Permit overcommission, but refuse obvious overcommits, and give root users more memory allocation than normal users.
- `1`: All memory requests are allowed to overcommit.
- `2`: Turn off overcommission. Memory requests will fail when the total memory commit reaches the size of the swap space plus a configurable percentage (50 by default) of RAM. This factor is modified changing `/proc/sys/vm/overcommit_ratio`.

When available memory is exhausted, the OOM-killer is invoked to decide which process(es) should be terminated to free up memory. The OOM-killer uses a "badness" score, which can be read from `/proc/<pid>/oom_score` to determine the order in which processes are terminated. The badness score can be manually adjusted using `oom_adj_score` in the same folder. Similarly to niceness scores, regular users can only increase the badness, while super users can increase or decrease the value.