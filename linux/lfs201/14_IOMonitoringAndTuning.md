# I/O Monitoring and Tuning

- [I/O Monitoring and Disk Bottlenecks](#io-monitoring-and-disk-bottlenecks)
- [`iostat`](#iostat)
- [`iotop`](#iotop)
- [`ionice`](#ionice)

## I/O Monitoring and Disk Bottlenecks

Disk performance is strongly influenced by other factors like memory and network hardware.

Generally speaking, a system can be considered I/O-bound when the CPU is idle waiting for I/O to complete, or when the network is waiting to clear buffers.

- These things can be misdiagnosed as memory or network problems; since memory performance will suffer if memory buffers take too long to fill and empty, and network throughput will suffer if I/O takes too long to complete.

## `iostat`

Widely used utility for monitoring I/O device activity.

Form:

```
iostat [options] [devices] [interval] [count]
```

## `iotop`

Like `top` but for I/O. Must be run with admin privileges.

## `ionice`

*DEPRECATED:* Only works when using CFQ I/O Scheduler, which was removed in Kernel 5.0.

Like `nice` but for I/O. Allows setting both the priority and the "scheduling class" for a process.

Scheduling classes are:

- Real Time: Gets first access to the disk, with priority defining how big a time slice each process gets. Can starve other processes.
- Best Effort: Access granted round-robin according to priority settings. This is the default scheduling class.
- Idle: No disk access unless no other process has asked for it for a defined period.
