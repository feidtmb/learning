# Neural Networks and Deep Learning

Personal notes and examples from the [Neural Networks and Deep Learning book](neuralnetworksanddeeplearning.com).

- [Chapter 1](#chapter-1)
  - [Perceptrons](#perceptrons)
  - [Sigmoid Neurons](#sigmoid-neurons)
  - [Architecture Of Neural Networks](#architecture-of-neural-networks)
  - [Simple Network To Classify Handwritten Digits](#simple-network-to-classify-handwritten-digits)
  - [Learning With Gradient Descent](#learning-with-gradient-descent)

## Chapter 1

### Perceptrons

A perceptron is a simple artificial neuron that takes a series of weights to produce a binary output. The output is determined by comparing the summation of the weights against a threshold value, with a 0 output if <= the threshold.

$$
\text{output \{ }^{0 \text{ if } \sum_{j} w_{j} x_{j} \leq \text{threshold}}_{1 \text{ if } \sum_{j} w_{j} x_{j} \gt \text{threshold}}
$$

Modifying the above, change the summation to the dot product of the weights and inputs, and let bias "b" = the negation of the threshold.

$$
w \cdot x \equiv \sum_{j} w_{j} x_{j}
$$

$$
b \equiv - \text{threshold}
$$

This lets us rewrite to the following, which expresses the ease of getting the neuron to "fire" (output a 1) depending on the neuron's bias (b):

$$
\text{output \{ }^{0 \text{ if } w \cdot x + b\leq 0}_{1 \text{ if } w \cdot x + b \gt 0}
$$

Perceptrons can be used to implement logic gates. For example, we can implement NAND by using a perceptron with two inputs, each with weights of -2, and a bias of 3. With this setup, inputs 00, 01, and 10 produce output 1, while input 11 produces output 0:

$$
00 = (-2 \times 0) + (-2 \times 0) + 3 \gt 0 = 1 \\
01 = (-2 \times 0) + (-2 \times 1) + 3 \gt 0 = 1 \\
10 = (-2 \times 1) + (-2 \times 0) + 3 \gt 0 = 1 \\
11 = (-2 \times 1) + (-2 \times 1) + 3 \leq 0 = 0 \\
$$

Because the NAND gate is universal for computation, so are perceptrons. The interesting thing is that we can define learning algorithms to automatically tune weights and biases in order to solve problems, without having to manually design a conventional circuit like we would if using normal NAND gates.

### Sigmoid Neurons

To help us implement learning algorithms, we want a small change in a single weight or bias to translate to a small change in the overall output from an artifical neuron network. This isn't what happens with perceptrons, since a small change could cause a neuron to totally flip from 0 to 1 and cause a complicated cascade of changes that affect the overall output significantly.

Sigmoid neurons solve this problem by changing a few things compared to perceptrons:

- Inputs aren't just 0 or 1, but are instead some value between 0 and 1.
- Output isn't just 0 or 1, but is instead the result of the *sigmoid function* (also called the *logistic function*).

Remembering that $e$ is Euler's number (2.718281828459045...), the sigmoid function is:

$$
\sigma (z) \equiv \frac 1 {1 + e^{-z}}
$$

And the output from a sigmoid neuron is:

$$
\sigma (w \cdot x + b)
$$

Written another way, the output from a sigmoid neuron is:

$$
\frac 1 {1 + \text{exp} (- \sum_j w_j x_j - b)}
$$

The plotted shape of the sigmoid function is a smoothed out version of a step function. When $z$ is very large the output approaches 1, and when $z$ is very negative the output approaches 0; just like it would be with a perceptron. (And in fact, if the sigmoid function were a slightly modified step function then we'd have a perceptron instead.) Sigmoid neuron behavior only significantly differs from perceptrons when $z$ is of modest size.

Note that the shape of the function is really what matters, meaning there are some other *activation functions* that can be used. The sigmoid function is used in this book because it's common and simplifies algebra when computing partial derivatives.

### Architecture Of Neural Networks

The layers in a neural network are referred to as the *input layer*, *hidden layers*, and *output layer*. For historical reasons, networks with multiple layers are sometimes called *multilayer perceptrons* or *MLPs* regardless of the actual kind of neuron in the network.

The design of networks' input and output layers is usually straightforward. Using the example of identifying the handwritten digit "9": if our input is a 64x64 greyscale image, then we could have an input layer of 4,096 neurons with each corresponding to a pixel's intensity; and our output layer could be a single neuron that produces a value > 0.5 if the image is of a 9.

The design of hidden layers is far less obvious and usually guided by design heuristics that will be covered later.

Networks that do not contain loops are called *feedforward* neural networks. This is currently the most common and powerful kind of network, but networks that do contain loops are closer to the way our brains work and may be able to solve some problems better than feedforward networks.

Networks with loops are called *recurrent* neural networks. These networks handle loops by limiting the duration of time a neuron can fire, and having that firing stimulate other neurons to potentially fire after a delay and also for a limited duration. This results in a cascade of neurons firing over time, and loops are accomodated by ensuring a neurons output can only affect its input at some later time and not instantaneously.

To limit scope, this book focuses on feedforward networks.

### Simple Network To Classify Handwritten Digits

Identifying handwritten digits can be split into two sub-problems: splitting the input into single digits, and identifying a single digit. The first sub-problem can be referred to as the *segmentation problem*, and we'll find that we can solve this more easily once we solve the second sub-problem, since we can use our confidence in identifying single digits to measure how well we solved the segmentation problem.

For the problem of identifying single digits, we'll use training data consisting of 28x28 pixel greyscale images. Our input layer will have one neuron per pixel for a total of 784 neurons. We'll use one hidden layer of $n$ neurons where $n = 15$, and an output layer of one neuron per possible digit for a total of 10 output neurons.

### Learning With Gradient Descent

We'll use the MNIST handwritten digits data set to train our network. This consists of 28x28 pixel greyscale images; with 60,000 in the training data set and 10,000 in the test data set.

Each training input $x$ is a 28x28 = 784-dimensional vector, and each output $y = y(x)$ is a 10-dimensional vector. For example, if $x$ depicts 6 then we want $y(x) = (0,0,0,0,0,0,1,0,0,0)$.

What we want is an algorithm that lets us find weights and biases so that the output from the network approximates our desired output for each training input. To quantify how well we're achieving this goal we define a *cost function* (also called a *loss function* or *objective function*):

$$
C(w,b) \equiv \frac 1 {2n} \sum_x || y(x) - a||^2
$$

The reason we use this function instead of directly inspecting outputs is to get a smooth function of the weights and biases in the network. If we were to look at the outputs directly, it'd often be the case that small changes to weights and biases don't affect the outputs at all; and that makes it more difficult to figure out how to improve performance.

In our above function, $w$ is the collection of all weights in the network, $b$ is all biases, $n$ is the total number of training inputs, $a$ is the vector of outputs when $x$ is input, and the sum is over all training inputs $x$. Be aware that $a$ depends on $x$, $w$, and $b$; but this isn't explicitly indicated to keep the notation simple. The notation $||v||$ denotes the usual length function for a vector $v$. We'll call $C$ the *quadratic cost function*, but it's also sometimes known as the *mean squared error* or *MSE*. Note that our function works for understanding the basics of learning in neural networks, but is not perfect and will be revisted later.

In our function, $C(w,b)$ is non-negative and approaches $0$ when $y(x) \approx a$ for all training inputs $x$. Therefore our training algorithm has done a good job if it can find weights and biases so that cost $C(w,b) \approx 0$. In order to find weights and biases that make the cost as small as possible, use an algorithm called *gradient descent*.

Skipping over the math, gradient descent can be thought of as taking small steps in the direction that does the most to immediately decrease cost, with the goal of finding the global minimum. This ends up being an approximation, and several things can go wrong that will be discussed later, but overall gradient descent may be the optimal way to search for the minimum. Unfortunately, using gradient descent with a large number of training inputs can take a long time, so an idea called *stochastic gradient descent* is often used to speed things up. The idea is simply to estimate calculations using a small sample of randomly chosen training inputs. The explicit process for using *stochastic gradient descent* to train a network is as follows: pick out a random "mini-batch" of training inputs, train, repeat until we've run out of training inputs. Once we've run out, we've completed one *epoch* of training. We then simply start over.
