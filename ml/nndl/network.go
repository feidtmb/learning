package nndl

import (
	"fmt"
	"math/rand"
)

// Network is a network of artificial neurons, which are fed an initial set of inputs in order to generate a final set
// of outputs.
type Network struct {
	// Layers are layers of nodes in the network, with each node defining its inputs and producing an output. The
	// topmost layer of nodes may only take inputs from the initial set of inputs provided to the network, but each
	// subsequent layer may take as inputs the initial set plus the outputs of any nodes in the above layers. The
	// outputs of the final layer of nodes are the outputs of the network as a whole.
	Layers [][]Node
}

// Node is a single node in a network of artificial neurons. It must have accurately defined inputs or it will cause the
// network to panic.
type Node struct {
	// Inputs is an ordered series of coordinates for other nodes in the network, whose outputs will be used as inputs
	// to this node. The first coordinate is the layer index, with index zero corresponding to the initial inputs to the
	// network. The second coordinate is the position of the node in the identified layer. For example, coordinates
	// {2, 0} point to the first node at the network's second layer of nodes; and coordinates {0, 4} point to the fifth
	// initial network input.
	//
	// The number of inputs must match the number of weights on the node's neuron or the node will panic. Further, the
	// coordinates of each input must be valid and not refer to a layer equal to or deeper than this node's layer;
	// otherwise the node will panic.
	Inputs [][2]int

	// Neuron is the artificial neuron used by this node to create an output from the inputs.
	Neuron Neuron
}

// NewRandomNetwork assembles a randomized network of the given kind of neurons that accepts the given number of inputs
// and produces the given number of outputs.
//
// Panics if max layers > max nodes, or if either is <= 0.
func NewRandomNetwork(neuronKind NeuronKind, numInputs int, numOutputs int, maxLayers int, maxNodes int) Network {
	if maxLayers > maxNodes {
		panic("max layers can't be greated that max nodes")
	}
	if maxLayers <= 0 || maxNodes <= 0 {
		panic("both max layers and max nodes must be positive")
	}

	layers := make([][]Node, rand.Intn(maxLayers)+1)
	numNodes := rand.Intn(maxNodes-len(layers)) + 1 + len(layers) // at least one node per layer

	// Distribute nodes through layers.
	for i := 0; i < numNodes; i++ {
		// We need at least one node per layer, so we'll fulfill that requirement before randomzing the remaining.
		if i < len(layers) {
			layers[i] = append(layers[i], Node{})
			continue
		}

		layerIdx := rand.Intn(len(layers))
		layers[layerIdx] = append(layers[layerIdx], Node{})
	}

	// For each node in each layer, assign random available inputs and a randomized neuron.
	var numAvailableInputs int
	for i := range layers {
		// Update number of available inputs at each layer.
		if i == 0 {
			numAvailableInputs = numInputs
		} else {
			numAvailableInputs += len(layers[i-1])
		}

		for j := range layers[i] {
			// Assign a random number of inputs up to the max available at this layer.
			inputs := make([][2]int, rand.Intn(numAvailableInputs)+1)
			for inputIdx := range inputs {
				// Get a random coordinate for a layer above this one.
				x := rand.Intn(i + 1)
				// Get a random coordinate for an input in the selected layer.
				var y int
				if x == 0 {
					y = rand.Intn(numInputs)
				} else {
					y = rand.Intn(len(layers[x-1]))
				}

				inputs[inputIdx] = [2]int{x, y}
			}

			// Assign the input to this node. Note that we may use the same input more than once.
			layers[i][j].Inputs = inputs
			// Assign a randomized neuron to this node, with number of weights based on number of inputs.
			layers[i][j].Neuron = NewRandomNeuron(neuronKind, len(inputs))
		}
	}

	return Network{
		Layers: layers,
	}
}

// Process takes an initial set of inputs to be fed through the network and eventually produce a final set of outputs.
func (n Network) Process(inputs ...float64) []float64 {
	// Output layers correspond to node layers plus the initial input layer.
	outputs := make([][]float64, len(n.Layers)+1)
	for i, nodes := range n.Layers {
		// Skip the first output layer, then build each subsequent layer corresponding to the number of nodes in that
		// layer.
		outputs[i+1] = make([]float64, len(nodes))
	}
	// Assign the inputs to the first output layer.
	outputs[0] = inputs

	// Process each layer, collecting outputs as we go.
	for layerIdx, layer := range n.Layers {
		for nodeIdx, node := range layer {
			inputs := make([]float64, 0, len(node.Inputs))
			for _, input := range node.Inputs {
				x, y := input[0], input[1]
				inputs = append(inputs, outputs[x][y])
			}
			outputs[layerIdx+1][nodeIdx] = node.Neuron.Process(inputs...)
		}
	}

	// Return the final output layer as our results.
	return outputs[len(n.Layers)]
}

// NewRandomNeuron returns a randomized neuron of the given kind that accepts the given number of inputs.
func NewRandomNeuron(kind NeuronKind, numInputs int) Neuron {
	weights := make([]float64, numInputs)
	for i := range weights {
		weights[i] = rand.Float64()*20 - 10 // [-10,10)
	}
	bias := rand.Float64() * -50 // (-50,0]

	switch kind {
	case NeuronKindPerceptron:
		return PerceptronWithBias{
			Weights: weights,
			Bias:    bias,
		}
	case NeuronKindSigmoid:
		return SigmoidNeuron{
			Weights: weights,
			Bias:    bias,
		}
	default:
		panic(fmt.Errorf("unrecognized neuron kind: %q", kind))
	}
}
