package nndl

import "testing"

func TestNetwork(t *testing.T) {
	t.Run("Adder", func(t *testing.T) {
		// This test is equivalent to the PerceptronWithBias adder test, but using a network instead of manually routing
		// outputs to inputs.

		nand := PerceptronWithBias{Weights: []float64{-2, -2}, Bias: 3}

		adder := Network{
			Layers: [][]Node{
				// Layer 1:
				{
					{Inputs: [][2]int{{0, 0}, {0, 1}}, Neuron: nand}, // output: 1,0
				},
				// Layer 2:
				{
					{Inputs: [][2]int{{0, 0}, {1, 0}}, Neuron: nand}, // output: 2,0
					{Inputs: [][2]int{{0, 1}, {1, 0}}, Neuron: nand}, // output: 2,1
				},
				// Layer 3:
				{
					{Inputs: [][2]int{{2, 0}, {2, 1}}, Neuron: nand}, // output: 3,0
					{Inputs: [][2]int{{1, 0}, {1, 0}}, Neuron: nand}, // output: 3,1
				},
			},
		}

		iss := [][]float64{
			{0, 0},
			{0, 1},
			{1, 0},
			{1, 1},
		}

		for _, is := range iss {
			var (
				wantSum   float64
				wantCarry float64
			)
			switch is[0] + is[1] {
			case 1:
				wantSum = 1
			case 2:
				wantCarry = 1
			}

			results := adder.Process(is[0], is[1])

			if len(results) != 2 {
				t.Fatalf("expected 2 results, got %d", len(results))
			}

			sum, carry := results[0], results[1]

			if sum != wantSum {
				t.Errorf("unexpected sum for inputs %f and %f: %f", is[0], is[1], sum)
			}
			if carry != wantCarry {
				t.Errorf("unexpected carry for inputs %f and %f: %f", is[0], is[1], carry)
			}
		}
	})
}
