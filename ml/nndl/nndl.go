// Package nndl is a personal learning project completed while reading Neural Networks and Deep Learning.
package nndl

import (
	"math/rand"
	"time"
)

// Neuron is the interface implemented by all artifical neurons in this package.
type Neuron interface {
	Process(inputs ...float64) float64
}

// NeuronKind identifies a kind of neuron.
type NeuronKind string

func init() {
	rand.Seed(time.Now().UnixNano())
}

func panicUnbalancedWeightsAndInputs(weights []float64, inputs []float64) {
	if len(weights) != len(inputs) {
		panic("unbalanced weights and inputs")
	}
}
