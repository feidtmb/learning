package nndl

func equivalentNetworks(numInputs int, numOutputs int, maxLayers int, maxNodes int) (perceptronNet Network, sigmoidNet Network) {
	perceptronNet = NewRandomNetwork(NeuronKindPerceptron, numInputs, numOutputs, maxLayers, maxNodes)

	sigmoidNet = Network{
		Layers: make([][]Node, len(perceptronNet.Layers)),
	}
	for i, layer := range perceptronNet.Layers {
		sigmoidNet.Layers[i] = make([]Node, len(layer))
		for j, node := range layer {
			perceptron := node.Neuron.(PerceptronWithBias)
			sigmoid := SigmoidNeuron{
				Weights: make([]float64, len(perceptron.Weights)),
				Bias:    perceptron.Bias,
			}
			copy(sigmoid.Weights, perceptron.Weights)
			sigmoidNet.Layers[i][j] = Node{
				Inputs: node.Inputs,
				Neuron: sigmoid,
			}
		}
	}

	return perceptronNet, sigmoidNet
}
