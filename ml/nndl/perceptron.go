package nndl

// NeuronKindPerceptron is the neuron kind corresponding to a perceptron.
const NeuronKindPerceptron NeuronKind = "perceptron"

// PerceptronWithThreshold is a simple artificial neuron that uses a threshold value compared to a weighted sum of
// inputs to produce a 0 or 1 output.
type PerceptronWithThreshold struct {
	Threshold float64
}

// Process takes a series of weighted inputs to produce a 0 or 1 output based on the sum of the weighted inputs compared
// to the perceptron's threshold. If the sum is <= the threshold, then the output will be 0; otherwise 1.
func (p PerceptronWithThreshold) Process(ws ...float64) float64 {
	var sum float64
	for _, w := range ws {
		sum += w
	}
	if sum > p.Threshold {
		return 1
	}
	return 0
}

// PerceptronWithBias is mathmatically equivalent to PerceptronWithThreshold. It uses a bias to represent ease of
// causing the neuron to "fire" (produce a 1 output), and operates on weights and inputs separetely instead of assuming
// weighted input will be provided.
type PerceptronWithBias struct {
	Weights []float64
	Bias    float64
}

// Process takes a series of inputs "x" that correspond to pre-defined weights "w", and produces a 0 or 1 output based
// on the dot product of "w" and "x" plus the perceptron's bias. If the result is <= 0, then the output will be 0;
// otherwise 1.
//
// Panics if weights and inputs are unbalanced.
func (p PerceptronWithBias) Process(inputs ...float64) float64 {
	panicUnbalancedWeightsAndInputs(p.Weights, inputs)

	// We could just convert the perceptron's bias to a threshold and use a PerceptronWithThreshold, but we'll instead
	// write equivalent code to demonstrate via tests that the approaches are interchangeable.

	var dotProduct float64
	for i, weight := range p.Weights {
		dotProduct += weight * inputs[i]
	}
	if dotProduct+p.Bias > 0 {
		return 1
	}
	return 0
}
