package nndl_test

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/feidtmb/learning/ml/nndl"
)

func ExamplePerceptronWithThreshold_basic() {
	type GoShoppingConsideration string
	const (
		wantFood     GoShoppingConsideration = "want_food"
		needFood     GoShoppingConsideration = "need_food"
		notBusy      GoShoppingConsideration = "not_busy"
		partnerGoing GoShoppingConsideration = "partner_going"
	)

	weights := map[GoShoppingConsideration]float64{
		wantFood:     92,
		needFood:     150,
		notBusy:      30,
		partnerGoing: 106,
	}

	inputs := map[GoShoppingConsideration]bool{
		wantFood:     true,
		needFood:     false,
		notBusy:      true,
		partnerGoing: false,
	}

	goShoppingNeuron := nndl.PerceptronWithThreshold{Threshold: 150}

	weightedInputs := make([]float64, 0, len(inputs))
	for consideration, yes := range inputs {
		var weight float64
		if yes {
			weight = weights[consideration]
		}
		weightedInputs = append(weightedInputs, weight)
	}

	fmt.Printf("Should shop: %v\n", goShoppingNeuron.Process(weightedInputs...))
	// output: Should shop: 0
}

func TestPerceptronWithThreshold(t *testing.T) {
	cases := []struct {
		threshold float64
		in        []float64
		want      float64
	}{
		{
			threshold: 10,
			in:        []float64{1, 5, 4},
			want:      0,
		},
		{
			threshold: 10,
			in:        []float64{12, -2},
			want:      0,
		},
		{
			threshold: 10,
			in:        []float64{5, 13, 1},
			want:      1,
		},
		{
			threshold: 10,
			in:        []float64{-20, 1, 2, 3, 15, 9},
			want:      0,
		},
		{
			threshold: 10,
			in:        []float64{-20, 1, 2, 3, 15, 10},
			want:      1,
		},
	}

	for _, c := range cases {
		p := nndl.PerceptronWithThreshold{Threshold: c.threshold}
		got := p.Process(c.in...)
		if got != c.want {
			t.Errorf("got %f with threshold %f and weighted inputs %v", got, c.threshold, c.in)
		}
	}
}

func TestPerceptronWithBias_equivalency(t *testing.T) {
	// We'll use random inputs, weights, and biases and compare results to PerceptronWithThreshold to test for
	// equivalency.
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	const n = 1000
	for i := 0; i < n; i++ {
		c := r.Intn(100) + 1
		inputs := make([]float64, c)
		weights := make([]float64, c)
		for i := 0; i < c; i++ {
			inputs[i] = float64(r.Intn(2))
			weights[i] = float64(r.Intn(21) - 10)
		}

		weightedInputs := make([]float64, len(weights))
		for i, weight := range weights {
			weightedInputs[i] = weight * inputs[i]
		}

		bias := float64(-r.Intn(50))

		p1 := nndl.PerceptronWithBias{Weights: weights, Bias: bias}
		p2 := nndl.PerceptronWithThreshold{Threshold: -bias}

		out1 := p1.Process(inputs...)
		out2 := p2.Process(weightedInputs...)

		if out1 != out2 {
			t.Fatal("perceptron with bias produced different result than perceptron with threshold")
		}
	}
}

func TestPerceptronWithBias_nandGate(t *testing.T) {
	nand := func(a, b float64) float64 {
		p := nndl.PerceptronWithBias{Weights: []float64{-2, -2}, Bias: 3}
		return p.Process(a, b)
	}

	iss := [][]float64{
		{0, 0},
		{0, 1},
		{1, 0},
		{1, 1},
	}
	for _, is := range iss {
		var want float64
		if !(is[0] > 0 && is[1] > 0) {
			want = 1
		}

		got := nand(is[0], is[1])

		if got != want {
			t.Errorf("got %f with inputs %f,%f", got, is[0], is[1])
		}
	}
}

func TestPerceptronWithBias_adder(t *testing.T) {
	nand := func(a, b float64) float64 {
		p := nndl.PerceptronWithBias{Weights: []float64{-2, -2}, Bias: 3}
		return p.Process(a, b)
	}

	adder := func(a, b float64) (float64, float64) {
		r1 := nand(a, b)
		r2 := nand(a, r1)
		r3 := nand(b, r1)
		sum := nand(r2, r3)
		carry := nand(r1, r1) // NOTE: If we don't want to allow inputs from the same source, we could instead use one input with a weight of -4.
		return sum, carry
	}

	iss := [][]float64{
		{0, 0},
		{0, 1},
		{1, 0},
		{1, 1},
	}

	for _, is := range iss {
		var (
			wantSum   float64
			wantCarry float64
		)
		switch is[0] + is[1] {
		case 1:
			wantSum = 1
		case 2:
			wantCarry = 1
		}

		sum, carry := adder(is[0], is[1])

		if sum != wantSum {
			t.Errorf("unexpected sum for inputs %f and %f: %f", is[0], is[1], sum)
		}
		if carry != wantCarry {
			t.Errorf("unexpected carry for inputs %f and %f: %f", is[0], is[1], carry)
		}
	}
}
