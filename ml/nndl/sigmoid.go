package nndl

import "math"

// NeuronKindSigmoid is the neuron kind corresponding to a sigmoid neuron.
const NeuronKindSigmoid NeuronKind = "sigmoid"

// SigmoidNeuron is an artificial neuron that uses a bias compared to a weighted sum of inputs to produce an output
// between 0 and 1.
type SigmoidNeuron struct {
	Weights []float64
	Bias    float64
}

// Process takes a series of inputs "x" that correspond to pre-defined weights "w", and produces an output between 0 and
// 1. The output is determined by the sigmoid function, with input being the dot product of "w" and "x" plus the
// neuron's bias.
//
// Panics if weights and inputs are unbalanced.
func (p SigmoidNeuron) Process(inputs ...float64) float64 {
	panicUnbalancedWeightsAndInputs(p.Weights, inputs)

	var dotProduct float64
	for i, weight := range p.Weights {
		dotProduct += weight * inputs[i]
	}
	z := dotProduct + p.Bias

	return 1 / (1 + math.Exp(-z))
}
