package nndl

import (
	"fmt"
	"math/rand"
	"testing"
)

func TestSigmoidNeuron(t *testing.T) {
	// We expect a network of sigmoid neurons to behave approximately the same as a network of perceptrons only when
	// "z" (w dot x + b) gets very large or very negative. We'll test this by introducing the positive constant "c"
	// and multiplying all weights and biases by it in order to adjust the size of "z" without affecting the output
	// of the perceptrons.
	//
	// Note that we should further ensure that no "z" is 0 since the behavior of perceptrons and sigmoid neurons is
	// essentially flipped for "z" of 0. This is unlikely to natually occur however, and is non-trivial to handle, so
	// we're not worrying about it in this test.
	const netLoops int = 100
	const numInputs int = 100

	t.Run("normal z perceptron and sigmoid networks not equivalent", func(t *testing.T) {
		perceptronNetwork, sigmoidNetwork := equivalentNetworks(numInputs, 1, 10, 100)

		for i := 0; i < netLoops; i++ {
			inputs := make([]float64, numInputs)
			for i := range inputs {
				inputs[i] = rand.Float64()
			}

			pout := perceptronNetwork.Process(inputs...)
			sout := sigmoidNetwork.Process(inputs...)

			if pout[0] == sout[0] {
				t.Fatal("expected different outputs from sigmoid and perceptron networks")
			}
		}
	})

	adjustZ := func(c float64, net Network) {
		for i, layer := range net.Layers {
			for j, node := range layer {
				switch node.Neuron.(type) {
				case PerceptronWithBias:
					perceptron := node.Neuron.(PerceptronWithBias)
					perceptron.Bias *= c
					for k := range perceptron.Weights {
						perceptron.Weights[k] *= c
					}
					net.Layers[i][j].Neuron = perceptron
				case SigmoidNeuron:
					sigmoid := node.Neuron.(SigmoidNeuron)
					sigmoid.Bias *= c
					for k := range sigmoid.Weights {
						sigmoid.Weights[k] *= c
					}
					net.Layers[i][j].Neuron = sigmoid
				default:
					panic(fmt.Errorf("unrecognized neuron kind: %T", node.Neuron))
				}
			}
		}
	}

	t.Run("very large z perceptron and sigmoid networks are equivalent", func(t *testing.T) {
		perceptronNetwork, sigmoidNetwork := equivalentNetworks(numInputs, 1, 10, 100)
		adjustZ(10000, perceptronNetwork)
		adjustZ(10000, sigmoidNetwork)

		for i := 0; i < netLoops; i++ {
			inputs := make([]float64, numInputs)
			for i := range inputs {
				inputs[i] = rand.Float64()
			}

			pout := perceptronNetwork.Process(inputs...)
			sout := sigmoidNetwork.Process(inputs...)

			if pout[0] != sout[0] {
				t.Fatal("expected same outputs from sigmoid and perceptron networks")
			}
		}
	})

	t.Run("very negative z perceptron and sigmoid networks are equivalent", func(t *testing.T) {
		perceptronNetwork, sigmoidNetwork := equivalentNetworks(numInputs, 1, 10, 100)
		adjustZ(-10000, perceptronNetwork)
		adjustZ(-10000, sigmoidNetwork)

		for i := 0; i < netLoops; i++ {
			inputs := make([]float64, numInputs)
			for i := range inputs {
				inputs[i] = rand.Float64()
			}

			pout := perceptronNetwork.Process(inputs...)
			sout := sigmoidNetwork.Process(inputs...)

			if pout[0] != sout[0] {
				t.Fatal("expected same outputs from sigmoid and perceptron networks")
			}
		}
	})
}
