use rand::Rng; // A trait.
use std::cmp::Ordering; // An enum.
use std::io; // A library.

fn main() {
    // `println!` is a macro.
    println!("Guess the number!");

    // The rand::Rng trait defines methods that random number generators implement (like gen_range), and this trait must
    // be in scope for us to use those methods.
    //
    // The 1..=100 argument is an inclusive range: [1..100], not [1..100).
    //
    // A number from 1 to 100 could be one of several types (i32, u32, i64, ...). Rust uses a default of i32 if it can't
    // infer the type more accurately, but in this program it will be a u32 because of later logic comparing our u32
    // guess to this variable.
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Please input your guess.");

        // Variables are immutable by default, thus the `mut` keyword.
        // A String is growable UTF-8 encoded text.
        // We're using the `new` "associated function" (method) to construct a new empty string.
        let mut guess = String::new();

        io::stdin() // If we hadn't imported std::io, we still could've used this function with `std::io::stdin`.
            .read_line(&mut guess) // `&` indicates a reference, and like variables these are immutable by default.
            .expect("Failed to read line"); // Method on the returned `Result` value, which causes a crash with the given message if the result indicates an error.

        // Shadow the guess variable with a u32 integer version, and bind it to the result of trimming and parsing our
        // string guess (assuming the Result enum returns its Ok variant).
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue, // The _ is a catchall value, indicating here that we want to match all Err values.
        };

        // Could also be written as `println!("You guessed: {}", guess);`.
        println!("You guessed: {guess}");

        // Compare guess to the secret number, and use match to decide what to do based on which Ordering enum variant
        // was returned. The match expression is said to be made up of arms.
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
