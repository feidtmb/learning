fn main() {
    explain_constants();
    explain_shadowing();
    explain_data_types();
    explain_expressions_and_statements();
    explain_functions();
    explain_loop();
    explain_while();
    explain_for();
}

fn explain_constants() {
    // Constants differ from immutable variables in that:
    //   - The type must be explicit.
    //   - They can not be set to values that can only be computed at run time.
    //   - They can be declared in any scope, including the global scope.
    const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;

    println!("Constant: {THREE_HOURS_IN_SECONDS}");
}

fn explain_shadowing() {
    // Variable shadowing, shown with a type change to illustrate why it may be preferable to a mutable variable:
    let spaces = "   ";
    println!("The unshadowed value of spaces is: {spaces}");
    let spaces = spaces.len();
    {
        let spaces = spaces * 2;
        println!("The value of spaces in the inner scope is: {spaces}");
    }
    println!("The value of spaces is: {spaces}");
}

fn explain_data_types() {
    // Data types:

    // Integers:
    let _: i32 = 0; // default type
    let _: i8 = 0; // smallest
    let _: i128 = 0; // largest
    let _: u16 = 0; // all sizes have unsigned variants
    let _: isize = 0; // size dependent on architecture
    let _ = 10_000_000; // decimal (with optional visual separators)
    let _ = 0b1111_0000; // binary (with optional visual separators)
    let _ = 0xff; // hex
    let _ = 0o77; // octal
    let _ = b'A'; // byte (u8 only)

    // Integer overflows happen using two's complement wrapping, and do not result in errors by default in code built
    // for release. The standard library has various methods for dealing with the possibility of overflows (wrapping_*,
    // checked_*, overflowing_*, saturating_*).

    // Floats:
    let _: f32 = 0.0;
    let _: f64 = 0.0; // default type.

    // Booleans:
    let _: bool = true;
    let _ = false;

    // Characters:
    let _: char = 'x';
    let _ = '😻';
    // Characters are four byte unicode scalar values.

    // Tuples:
    let tup = (500, "hello", 6.4); // fixed length, with each position having a type
    println!("tup: {}, {}, {}", tup.0, tup.1, tup.2);
    let (x, y, z) = tup; // destructuring
    println!("x, y, z: {x}, {y}, {z}");
    let _tup: () = ();
    // The empty tuple seen above (with its optional explicit type) has a special name: "unit". This is the implicit
    // return value from expressions that don't explicitly return anything.

    // Arrays:
    let _x = ['a', 'b', 'c']; // fixed length, same type for all elements
    let x: [i32; 5] = [1, 2, 3, 4, 5]; // type is written as the type of each element followed by the size of the array
    println!("elements: {} {} {} {} {}", x[0], x[1], x[2], x[3], x[4]);
    let _x = [3; 5]; // equivalent to [3, 3, 3, 3, 3]

    // Attempting to access an out of bounds array index will result in a panic.
}

fn explain_expressions_and_statements() {
    let y = {
        let x = 3; // A statement.

        // The lack of a semicolon below makes this an expression, resulting in the evaluated value being returned from
        // the block (and y equaling 4). Adding a semicolon would make this a statement, resulting in no value being
        // returned from the block, in which case y would equal the implicit return value of the empty tuple (called a
        // unit).
        x + 1
    };
    println!("The value of y is: {y}");

    // Because if is an expression, it can be used in let statements.
    let condition = true;
    let number = if condition { 5 } else { 6 }; // return values must be of the same type
    println!("The value of number is: {number}");
}

fn explain_functions() {
    println!("add_one(5) = {}", add_one(5));
    println!("five() = {}", five());
}

fn add_one(x: i32) -> i32 {
    x + 1 // implicit return (most common)
}

fn five() -> i32 {
    return 5; // explicit return (less common)
}

fn explain_loop() {
    // Simple loop:
    loop {
        println!("looping!");
        break;
    }

    // Loop with a return value:
    let mut counter = 0;
    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2; // value returned from the loop
        }
    };
    println!("The loop result is {result}"); // 20

    // Loop labels (must begin with a single quote):
    counter = 0;
    'counting_up: loop {
        let mut remaining = 2;
        loop {
            if remaining == 0 {
                break; // stop decrementing remaining
            }
            if counter == 3 {
                break 'counting_up; // stop incrementing counter
            }
            remaining -= 1;
        }
        counter += 1;
    }
    println!("End count = {counter}");
}

fn explain_while() {
    // (Note that this example is considered less safe and less concise than using for with a range.)
    let mut number = 3;

    while number != 0 {
        println!("{number}!");
        number -= 1;
    }

    println!("LIFTOFF!!!");
}

fn explain_for() {
    let a = [10, 20, 30];
    for element in a {
        println!("the value is: {element}");
    }

    // More idiomatic version of the liftoff example in explain_while:
    for number in (1..4).rev() {
        println!("{number}!");
    }
    println!("LIFTOFF!!!");
}
