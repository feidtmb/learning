fn main() {
    explain_ownership();
    explain_references_and_borrowing();
    explain_slices();
}

fn explain_ownership() {
    // The main purpose of ownership is to manage heap data.

    // Ownership rules:
    // - Each value in Rust has an owner.
    // - There can only be one owner at a time.
    // - When the owner goes out of scope, the value will be dropped.

    {
        // Immutable, hardcoded directly into the final executable.
        let _s = String::from("hello");
    }
    {
        // Mutable, allocated on the heap.
        let mut s = String::from("hello");
        s.push_str(", world!");
        // Allocated memory is automatically freed with `drop` when the variable goes out of scope.
    }
    {
        // Binds the value 5 to x, and makes a copy of it to bind to y.
        // Both are simple values with a known, fixed size; and so are pushed onto the stack.
        let x = 5;
        let y = x;
        println!("x = {}, y = {}", x, y);
    }
    {
        // A String is made up of three parts stored on the stack:
        //   - a pointer to the memory that holds the contents of the string
        //   - a length (in bytes)
        //   - a capacity (in bytes)
        // The pointer points to a location in the heap.
        let s1 = String::from("hello");
        // When we copy s1 here, we copy the String data including the pointer. The data on the heap that the pointer
        // points to is not copied.
        let s2 = s1;
        // Since both variables refer to the same heap data, we could encounter a double free bug if they both attempt
        // to free the same memory. Rust avoids this by considering s1 invalid after it's copied to s2. This is referred
        // to as a "move" instead of a "shallow copy".
        println!("{}, world!", s2); // Using s1 would result in a compiler error because of the invalidated reference.
    }
    {
        // Clone performs a deep copy of both stack and heap data.
        let s1 = String::from("hello");
        let s2 = s1.clone();
        println!("s1 = {}, s2 = {}", s1, s2);
    }
    {
        // Rust has Copy and Drop traits, which are mutually exclusive.
        //
        // The Copy trait is for types that are stored on the stack, and results in them being trivially copied instead
        // of moved. Integers for example are annotated with the Copy trait, which is why we were able to copy x to y
        // and still use both values in our earlier example.
        //
        // The Drop trait is for types that need something to happen when the variable goes out of scope. Attempting to
        // annotate a type with the Copy trait when it or any of its parts has implemented the Drop trait will result in
        // a compile error.
        //
        // The following are some types that implement Copy:
        //   - Integers
        //   - Booleans
        //   - Floating-point numbers
        //   - Character
        //   - Tuples if they only contain types that implement Copy.
    }
    {
        // Passing an argument to a function is like assigning it to a variable: it performs a move or copy.

        let s = String::from("hello");
        takes_ownership(s);
        // s is no longer valid here, since it was moved.

        let x = 5;
        makes_copy(x);
        // x is still valid here, since it implements Copy.
        println!("post makes_copy: {}", x);
    }
    {
        // Returning values from functions also transfers ownership.

        let s1 = gives_ownership(); // ownership moved to s1
        let s2 = String::from("hello");
        let s3 = takes_and_gives_back_ownership(s2);
        // s2 is now invalid, its ownership was moved to the argument, then moved again to s3
        println!("s1: {}, s3: {}", s1, s3);

        // When we go out of scope, s1 and s3 are dropped.
    }
}

fn takes_ownership(s: String) {
    println!("takes_ownership: {}", s);
}

fn makes_copy(x: i32) {
    println!("makes_copy: {}", x);
}

fn gives_ownership() -> String {
    let s = String::from("yours");
    s
}

fn takes_and_gives_back_ownership(s: String) -> String {
    s
}

fn explain_references_and_borrowing() {
    // Unlike a pointer, a reference is guaranteed to point to a valid value of a particular type for the life of that
    // reference.

    // References are immutable by default.
    let s1 = String::from("hello");
    let len = calculate_length(&s1); // s1 is "borrowed" (no ownership change)
    println!("The length of '{}' is {}.", s1, len);

    // If there's a mutable reference to a value, there are not allowed to be any other references to that value. This
    // allows Rust to prevent data races at compile time.
    let mut s2 = String::from("hello");
    let r1 = &mut s2; // No other reference to s2 is allowed until this one stops being used.
    change(r1);
    // It's okay to define another reference to s2 here because r1 isn't used beyond this point.
    let _r2 = &s2;
    println!("The length of '{}' is {}.", s2, calculate_length(&s2));

    // Rust prevents dangling references at compile time (references to data that has gone out of scope).
}

fn calculate_length(s: &String) -> usize {
    // s can't be modified here since references are immutable by default
    s.len()
}

fn change(s: &mut String) {
    // s is mutable here
    s.push_str(" world");
}

fn explain_slices() {
    // A slice is like a reference, and does not have ownership of the collection it references.
    // Internally, a slice consists of a pointer and a length.
    // The type that signifies a string slice is &str.

    {
        let s = String::from("hello world");
        let hello = &s[0..5]; // the zero index is optional: &s[..5]
        let world = &s[6..11]; // the end index is optional: &[6..]
        println!("{} {}", hello, world);

        let slice = &s[..]; // both zero and end indices are optional
        println!("{}", slice);
    }

    {
        let mut s = String::from("hello world");

        let first_word = nth_word(&s, 0);
        // Attempting to empty the string before the slice stops being used is a compiled error.
        // s.clear();
        println!("first word: {}", first_word);

        let second_word = nth_word(&s, 1);
        println!("second word: {}", second_word);

        // We can empty the string now since the slices are done being used.
        s.clear();
        println!("cleared s: {}", s);
    }

    {
        // String literals are slices.
        let _s = "Hello, world!"; // s is a &str that points to the point in the binary where the data is hard coded.
    }

    {
        // Non-string slices have types like &[i32]
        let a = [1, 2, 3, 4, 5];
        let _slice = &a[1..3]; // value is &[2, 3]
    }
}

// For the type of s, we could use either a slice (&str) or a reference to String (&String). (This works thanks to deref
// coercions.) A slice is somewhat preferable because it's more flexible (we could pass either a &String or a &str).
fn nth_word(s: &str, n: usize) -> &str {
    let bytes = s.as_bytes();
    let mut m = 0;
    let mut i = 0;

    for (j, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            if m == n {
                return &s[i..j];
            }
            i = j + 1;
            m += 1;
        }
    }

    // Return the last word, even if it's not the nth.
    &s[i..]
}
