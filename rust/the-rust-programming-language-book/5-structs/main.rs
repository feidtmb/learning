fn main() {
    explain_basics();
    explain_tuple_structs();
    explain_unit_like_structs();
    explain_debugging();
    explain_methods();
}

fn explain_basics() {
    struct User {
        active: bool,
        username: String,
        email: String,
        sign_in_count: u64,
    }

    fn describe_user(u: &User) {
        println!(
            "email: {}; username: {}; active: {}; sign in count: {}",
            u.email, u.username, u.active, u.sign_in_count
        );
    }

    let email = String::from("someone@example.com");
    let mut user1 = User {
        email, // Field init shorthand, possible because variable matches field name.
        sign_in_count: 1,
        username: String::from("someusername123"),
        active: true,
    };
    user1.sign_in_count += 1;
    describe_user(&user1);

    // Struct update syntax:
    let user2 = User {
        email: String::from("another@example.com"),
        ..user1
    };
    describe_user(&user2);

    // Because we didn't provide a new value for user2.username, a move was performed on the value, which made it so
    // that we're not able to use user1 anymore (ownership rules).
    // describe_user(&user1);

    // Note that we can't store references to data owned elsewhere inside structs without specifying "lifetimes"
    // (discussed later).
}

fn explain_tuple_structs() {
    // A tuple struct is just like a normal tuple except the tuple has a name.
    struct Color(i32, i32, i32);

    let red = Color(255, 0, 0);
    println!("red is rgb: {},{},{}", red.0, red.1, red.2);
}

fn explain_unit_like_structs() {
    // A struct without any fields is like the unit type.
    //
    // This is useful for example if we want to implement a trait on the type but don't need to store any data in
    // the type itself. (For instance, an AlwaysEqual type that we want to be equal to every instance of any other
    // type, for testing purposes.)
    struct AlwaysEqual;
    let _subject = AlwaysEqual;
}

fn explain_debugging() {
    // This derive thing is called an "attribute", and Debug is called a "trait". Here we're opting into default debug
    // functionality to support printing info about this struct.
    #[derive(Debug)]
    struct Rectangle {
        width: u32,
        height: u32,
    }

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };
    println!("rect1 is {} by {}", rect1.width, rect1.height);
    println!("rect1 is {:?}", rect1); // {:?} uses the Display trait (implemented by Debug in this case) to print information about the struct.
    println!("rect1 is {:#?}", rect1); // {:#?} breaks up the print with newlines, useful for larger structs.

    // Instead of the println! macro, we could use the dbg! macro to:
    //   - print to stderr instead of stdout
    //   - include source code location information
    //   - take and return ownership of the expression
    dbg!(&rect1);

    let scale = 2;
    let rect2 = Rectangle {
        width: dbg!(30 * scale), // This works because dbg! takes and then returns ownership of the expression.
        height: 50,
    };
    dbg!(&rect2);
}

fn explain_methods() {
    struct Rectangle {
        width: u32,
        height: u32,
    }

    // This is called an "implementation block".
    // All functions defined in this block are called "associated functions".
    impl Rectangle {
        // Methods' first parameter is always `self`, which represents the instance of the type. Here it's actually
        // shorthand for `self: &Self`, where `Self` is an alias for the type that the implementation block is for.
        // Methods can take ownership of `self`, borrow it immutably (like here) or borrow it mutably; just like any
        // other parameter.
        fn area(&self) -> u32 {
            self.width * self.height
        }

        // Methods and fields can share the same name.
        fn width(&self) -> bool {
            self.width > 0
        }

        // This is not a method because it doesn't take `self` as the first parameter, but it's still associated with
        // the other functions (methods) in this block. These kinds of associated functions are usually constructors, as
        // is the case here.
        fn square(size: u32) -> Self {
            Self {
                width: size,
                height: size,
            }
        }
    }

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    // Rust automatically references or de-references as needed when calling a method. For example, instead of having
    // to write `(&rect1).area()` we can just write `rect1.area()` because Rust knows what to do based on the method's
    // `self` parameter.

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );

    if rect1.width() {
        println!(
            "The rectangle has a non-zero width of {} pixels.",
            rect1.width
        );
    }

    // Associated functions that aren't methods are called using `::` syntax.
    let square1 = Rectangle::square(10);
    println!(
        "The area of the square is {} square pixels.",
        square1.area()
    );
}
