fn main() {
    explain_basics();
    explain_option();
    explain_match();
    explain_if_let();
}

fn explain_basics() {
    // Basic enum.
    {
        enum IPAddrKind {
            V4,
            V6,
        }

        let _four = IPAddrKind::V4;
        let _six = IPAddrKind::V6;
    }

    // Enum with associated values.
    {
        enum IPAddrKind {
            V4(u8, u8, u8, u8),
            V6(String),
        }

        // We automatically get constructor functions with this kind of enum.
        let _home = IPAddrKind::V4(127, 0, 0, 1);
        let _loopback = IPAddrKind::V6(String::from("::1"));
    }

    // Variety enum.
    {
        enum Message {
            Quit,                       // No associated values.
            Move { _x: i32, _y: i32 },  // Named fields, like a struct.
            Write(String),              // Single associated String.
            ChangeColor(i32, i32, i32), // Three i32 values.
        }

        let _m1 = Message::Quit;
        let _m2 = Message::Move { _x: 5, _y: 10 };
        let _m3 = Message::Write(String::from("hello"));
        let _m4 = Message::ChangeColor(255, 0, 120);

        // Equivalent structs, though they're not grouped together under the `Message` type like the enum is.
        struct QuitMessage; // unit struct
        struct MoveMessage {
            _x: i32,
            _y: i32,
        } // typical struct
        struct WriteMessage(String); // tuple struct
        struct ChangeColorMessage(i32, i32, i32); // tuple struct

        let _qmsg = QuitMessage;
        let _mmsg = MoveMessage { _x: 5, _y: 10 };
        let _wmsg = WriteMessage(String::from("hello"));
        let _cmsg = ChangeColorMessage(255, 0, 120);
    }

    // Enums can have implementation blocks like structs.
    {
        enum Message {
            Write(String),
        }

        impl Message {
            fn call(&self) {
                // ...
            }
        }

        let m = Message::Write(String::from("hello"));
        m.call();
    }
}

fn explain_option() {
    // Rust does not have a Null type, and instead uses the Option enum's None variant. This is intended to force
    // explicit handling of cases where values can be null and eliminate bugs related to false assumptions.

    // The Option enum is defined in the stdlib and both it and its variants are included in the prelude:
    /*
        enum Option<T> {
            None,
            Some(T),
        }
    */
    // The <T> syntax is for a generic type parameter.

    let _some_number = Some(5);
    let _some_char = Some('e');
    let _absent_number: Option<i32> = None;

    // Option has various methods: https://doc.rust-lang.org/std/option/enum.Option.html
}

fn explain_match() {
    // Basic syntax.
    {
        #[derive(Debug)]
        enum UsState {
            Alabama,
            // ...
        }

        enum Coin {
            Penny,
            Nickel,
            Dime,
            Quarter(UsState),
            HalfDollar,
        }

        fn value_in_cents(coin: Coin) -> u8 {
            // A match expression has "arms", and the `=>` operator separates the pattern and the matching code.
            match coin {
                Coin::Penny => 1,
                Coin::Nickel => 5,
                Coin::Dime => 10,
                Coin::Quarter(state) => {
                    println!("Quarter from {:?}!", state);
                    25
                }
                Coin::HalfDollar => 50,
            }
        }

        println!("{}", value_in_cents(Coin::Penny));
        println!("{}", value_in_cents(Coin::Nickel));
        println!("{}", value_in_cents(Coin::Dime));
        println!("{}", value_in_cents(Coin::Quarter(UsState::Alabama)));
        println!("{}", value_in_cents(Coin::HalfDollar));
    }

    // Match with Option.
    {
        fn plus_one(x: Option<i32>) -> Option<i32> {
            match x {
                None => None,
                Some(i) => Some(i + 1),
            }
        }

        let five = Some(5);
        let _six = plus_one(five);
        let _none = plus_one(None);
    }

    // Catch alls.
    {
        let mut dice_roll = 9;
        match dice_roll {
            3 => println!("You got a fancy hat!"),
            7 => println!("You lost a fancy hat!"),
            other => println!("Move {} spaces.", other), // The variable name `other` used here is arbitrary.
        }

        dice_roll = 5;
        match dice_roll {
            3 => println!("You got a fancy hat!"),
            7 => println!("You lost a fancy hat!"),
            _ => println!("Nothing happened..."), // The `_` is used because we don't need the value.
        }

        dice_roll = 8;
        match dice_roll {
            3 => println!("You got a fancy hat!"),
            7 => println!("You lost a fancy hat!"),
            _ => (), // The unit value (empty tuple) acts as a no-op.
        }
    }
}

fn explain_if_let() {
    // If let is syntax sugar for a match statement.
    {
        // Verbose:
        let mut config_max = Some(3u8);
        match config_max {
            Some(max) => println!("The maximum is configured to be {}.", max),
            None => (),
        }

        // Concise:
        config_max = Some(5u8);
        if let Some(max) = config_max {
            println!("The maximum is configured to be {}.", max);
        }
    }
}
