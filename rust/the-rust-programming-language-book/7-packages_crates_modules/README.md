# Chapter 7: Packages, Crates, And Modules

The packages in this folder contains multiple crates: A library crate and a few binary crates.

The `cargo run` command will execute the code in the main binary crate (`src/main.rs`).

## Packages and Crates

Crates:

- are the smallest amount of code that the Rust compiler considers at a time (even when using `rustc`)
- can come in one of two forms: a binary crate or a library crate
- library crates don't have a `main` function and don't compile to executables

Packages:

- are a bundle of one or more crates, and contain a `Cargo.toml` file
- can contain any number of binary crates and at most one library crate
- cargo convention is to use `src/main.rs` as the root of a binary crate and `src/lib.rs` as the root of a library crate, with any additional binary crates defined in `src/bin`

## Modules

New modules can be declared in the crate root with `mod <name>;` or `mod <name> { <code> }`.

The compiler will look for module source code in the following places:

- inline: `mod <name> { <code> }`
- in a file named after the module: `src/<name>.rs`
- in a file called `mod.rs` in a folder named after the module: `src/<name>/mod.rs`

Submodules behave the same, but are declared outside of the crate root. As before, source code can be:

- inline
- in `src/<name>/<sub>.rs`
- in `src/<name>/<sub>/mod.rs`
