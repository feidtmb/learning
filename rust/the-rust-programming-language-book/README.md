# The Rust Programming Language Book

## References

The Book: https://doc.rust-lang.org/book/title-page.html

Standard Library: https://doc.rust-lang.org/std/index.html

Language Reference: https://doc.rust-lang.org/reference/introduction.html

Crate Registry: https://crates.io

Online Crate Documentation: https://docs.rs/

## Quick Notes

New project:

```sh
cargo new <package-name>
```

Build:

```sh
# If --release is not specified, an unoptimized debug executable is built
cargo build [--release]
# For non-Cargo projects, use: rustc <file>
```

Run:

```sh
cargo run
# For non-Cargo projects, use built executable
```

Check that code can compile:

```sh
cargo check
```

Update dependencies:

```sh
cargo update
```

Build and open docs:

```sh
cargo doc --open
```

Format source code:

```sh
rustfmt <file>
```
