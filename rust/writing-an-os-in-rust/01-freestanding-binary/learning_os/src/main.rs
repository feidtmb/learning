// Exclude the standard library, which depends on C and OS libraries.
#![no_std]

// Don't use the standard entry point chain, which depends on C.
#![no_main]

use core::panic::PanicInfo;

// Panic is a diverging function that should never return, thus the returned "never" type `!` (read here as "diverges").
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    // Not much we can do at this stage, so just loop indefinitely to satisfy the never return condition.
    loop {}
}

// Disabling the standard entry point chain means we also lose the runtime that normally calls our `main` function.
// Instead of `main` we have to write a function to use when overwriting the default OS entry point.
//
// - The `#[no_mangle]` attribute disables name mangling to ensure the compiler keeps the name `_start` for this
// function.
// - `extern "C"` tells the compiler to use the C calling convention instead of the unspecified Rust calling convention.
// - The function name `_start` is the linux default entry point name, and the default used by most systems (and also
// used by the LLVM compiler that we use later).
#[no_mangle]
pub extern "C" fn _start() -> ! {
    loop {}
}
