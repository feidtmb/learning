# Writing An OS In Rust

Following along with https://os.phil-opp.com/

## Quick Notes

I'm using the Rust 2021 edition, stable channel.

## Per Chapter Notes

### 01 Freestanding Binary

Project created with `cargo new learning_os --bin --edition 2021`.

Stack unwinding disabled by changing the panic strategy to "abort" in `Cargo.toml`, which removes the need for the `eh_personality` language item.

Compiler target with no underyling operating system added to `rustup` with `rustup target add thumbv7em-none-eabihf`. Project initially built for a bare metal target system with `cargo build --target thumbv7em-none-eabihf`. (In 02, a different target is used.)

- `thumbv7em-none-eabihf` describes an embedded ARM system, though the only important thing for our use case is that it has no underlying OS.

### 02 Minimal Rust Kernel

Created a custom compiler target in `x86_64-blog_os.json`.

- Shares some common fields with the [target triple](https://clang.llvm.org/docs/CrossCompilation.html#target-triple) `x86_64-unknown-linux-gnu`, but has the OS set to `none`.
- Uses the cross-platform [LDD](https://lld.llvm.org/) linker that ships with Rust.
- Uses the abort panic strategy to disable [stack unwinding](https://www.bogotobogo.com/cplusplus/stackunwinding.php) (this also lets us remove that from `Cargo.toml`).
- Disables [the "red zone" stack pointer optimization](https://os.phil-opp.com/red-zone/) because it would cause stack corruption when handling interrupts.
- Disables the `mmx` and `sse` features used for [SIMD (Single Instruction Multiple Data)](https://en.wikipedia.org/wiki/SIMD) instructions, since these harm performance when used in OS kernels. Disabling them here doesn't disable them for applications running on top of the kernel.
- Enables the `soft-float` feature, which emulates floating point operations through software functions based on normal integers. This is required because SIMD registers are needed for floating point operations on x86_64.